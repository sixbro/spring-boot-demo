package com.sixbro.file.storage.service;

import com.sixbro.file.storage.entity.FileDocument;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2022/1/16 14:12
 */
public interface FileService {

    FileDocument upload(MultipartFile file) throws IOException;

    List<FileDocument> upload(List<MultipartFile> files) throws IOException;
}
