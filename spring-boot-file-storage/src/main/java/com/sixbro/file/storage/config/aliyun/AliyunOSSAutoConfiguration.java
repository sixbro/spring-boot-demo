package com.sixbro.file.storage.config.aliyun;

import com.aliyun.oss.ClientConfiguration;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.common.comm.Protocol;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PreDestroy;

/**
 * <p>
 * aliyun oss 自动配置
 * </p>
 *
 * @Author: Mr.Lu
 * @Since: 2020/6/17 14:34
 */
@Configuration
@EnableConfigurationProperties(OSSProperties.class)
public class AliyunOSSAutoConfiguration {

    private OSSClient ossClientForUpload;

    private OSSClient ossClientForManager;

    @PreDestroy
    public void close() {
        if (this.ossClientForUpload != null) {
            this.ossClientForUpload.shutdown();
        }
        if (this.ossClientForManager != null) {
            this.ossClientForManager.shutdown();
        }
    }

    @Bean(name = "clientConfiguration")
    @ConditionalOnMissingBean
    public ClientConfiguration clientConfiguration(OSSProperties ossProperties) {
        OSSProperties.Client client = ossProperties.getClient();
        ClientConfiguration configuration = new ClientConfiguration();
        configuration.setMaxConnections(client.getMaxConnections());
        configuration.setSocketTimeout(client.getSocketTimeout());
        configuration.setConnectionTimeout(client.getConnectionTimeout());
        configuration.setConnectionRequestTimeout(client.getConnectionRequestTimeout());
        client.setIdleConnectionTime(client.getIdleConnectionTime());
        configuration.setMaxErrorRetry(client.getMaxErrorRetry());
        configuration.setSupportCname(client.isSupportCname());
        configuration.setSLDEnabled(client.isSldEnabled());
        if (Protocol.HTTP.toString().equals(client.getProtocol())) {
            configuration.setProtocol(Protocol.HTTP);
        } else if (Protocol.HTTPS.toString().equals(client.getProtocol())) {
            configuration.setProtocol(Protocol.HTTPS);
        }
        configuration.setUserAgent(client.getUserAgent());

        return configuration;
    }

    @Bean(name = "ossClientForUpload")
    public OSSClient ossClientForUpload(OSSProperties oSSProperties, ClientConfiguration clientConfiguration) {
        ossClientForUpload = new OSSClient(oSSProperties.getEndpointUpload(), oSSProperties.getAccessKeyId(), oSSProperties.getAccessKeySecret(), clientConfiguration);
        return ossClientForUpload;
    }

    @Bean(name = "ossClientForManager")
    public OSSClient ossClientForManager(OSSProperties oSSProperties, ClientConfiguration clientConfiguration) {
        ossClientForManager = new OSSClient(oSSProperties.getEndpointManager(), oSSProperties.getAccessKeyId(), oSSProperties.getAccessKeySecret(), clientConfiguration);
        return ossClientForManager;
    }
}
