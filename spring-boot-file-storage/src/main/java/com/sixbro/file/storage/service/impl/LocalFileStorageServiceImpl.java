package com.sixbro.file.storage.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.sixbro.file.storage.entity.FileDocument;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2022/1/16 14:53
 */
@Service
@Slf4j
public class LocalFileStorageServiceImpl extends AbstractFileStorageService {

    @Value("${spring.servlet.multipart.location}")
    private String fileTempPath;

    @Override
    protected void uploadHandler(FileDocument document, MultipartFile file) throws IOException {
        String rawFileName = StrUtil.subBefore(document.getOriginalFilename(), ".", true);
        String localFilePath = StrUtil.appendIfMissing(fileTempPath, "/") + rawFileName + "-" + DateUtil.current(false) + "." + document.getFiletype();
        file.transferTo(new File(localFilePath));
        log.error("【文件上传至本地】绝对路径：{}", localFilePath);
    }
}
