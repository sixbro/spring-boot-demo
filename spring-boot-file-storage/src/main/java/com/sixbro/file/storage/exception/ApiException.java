package com.sixbro.file.storage.exception;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2022/1/16 14:38
 */
public class ApiException extends RuntimeException {

    public ApiException(String message) {
        super(message);
    }
}
