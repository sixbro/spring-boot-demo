package com.sixbro.file.storage.entity;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2022/1/16 14:18
 */
@Data
@Builder
@Accessors(chain = true)
public class FileDocument {

    /**
     * 原文件名称
     */
    private String originalFilename;

    /**
     * 原文件后缀名
     */
    private String originalFiletype;

    /**
     * 新文件名称
     */
    private String filename;

    /**
     * 文件后缀名
     */
    private String filetype;

    /**
     * 文件大小
     */
    private long size;

    /**
     * 文件MD5值
     */
    private String fileMd5;

    /**
     * 文件内容
     */
    private byte[] content;

    /**
     * 文件类型
     */
    private String contentType;

    /**
     * 文件浏览路径
     */
    private String url;

    /**
     * 文件描述
     */
    private String description;

    /**
     * 大文件管理GridFS的ID
     */
    private String gridfsId;
}
