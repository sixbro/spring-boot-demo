package com.sixbro.file.storage.config.aliyun;

import com.aliyun.oss.common.comm.Protocol;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * <p>
 * oss 配置
 * </p>
 *
 * @Author: Mr.Lu
 * @Since: 2020/6/17 14:30
 */
@ConfigurationProperties(OSSProperties.OSS_PREFIX)
public class OSSProperties {
    public static final String OSS_PREFIX = "app.oss";

    private String endpointUpload;
    private String endpointManager;
    private String accessKeyId;
    private String accessKeySecret;

    private Client client;

    public String getEndpointUpload() {
        return endpointUpload;
    }

    public OSSProperties setEndpointUpload(String endpointUpload) {
        this.endpointUpload = endpointUpload;
        return this;
    }

    public String getEndpointManager() {
        return endpointManager;
    }

    public OSSProperties setEndpointManager(String endpointManager) {
        this.endpointManager = endpointManager;
        return this;
    }

    public String getAccessKeyId() {
        return accessKeyId;
    }

    public OSSProperties setAccessKeyId(String accessKeyId) {
        this.accessKeyId = accessKeyId;
        return this;
    }

    public String getAccessKeySecret() {
        return accessKeySecret;
    }

    public OSSProperties setAccessKeySecret(String accessKeySecret) {
        this.accessKeySecret = accessKeySecret;
        return this;
    }

    public Client getClient() {
        return client;
    }

    public OSSProperties setClient(Client client) {
        this.client = client;
        return this;
    }

    static class Client {
        // 允许打开的最大HTTP连接数。默认为1024
        private int maxConnections = 1024;
        // Socket层传输数据的超时时间（单位：毫秒）。默认为50000毫秒
        private int socketTimeout = 50000;
        // 建立连接的超时时间（单位：毫秒）。默认为50000毫秒
        private int connectionTimeout = 50000;
        // 从连接池中获取连接的超时时间（单位：毫秒）。默认不超时
        private int connectionRequestTimeout;
        // 如果空闲时间超过此参数的设定值，则关闭连接（单位：毫秒）。默认为60000毫秒
        private long idleConnectionTime = 60000;
        // 请求失败后最大的重试次数。默认3次
        private int maxErrorRetry = 3;
        // 是否支持CNAME作为Endpoint，默认支持CNAME
        private boolean supportCname = true;
        // 是否开启二级域名（Second Level Domain）的访问方式，默认不开启
        private boolean sldEnabled = false;
        // 连接OSS所采用的协议（HTTP/HTTPS），默认为HTTP
        private String protocol = Protocol.HTTP.toString();
        // 用户代理，指HTTP的User-Agent头。默认为”aliyun-sdk-java”
        private String userAgent = "aliyun-sdk-java";

        public int getMaxConnections() {
            return maxConnections;
        }

        public Client setMaxConnections(int maxConnections) {
            this.maxConnections = maxConnections;
            return this;
        }

        public int getSocketTimeout() {
            return socketTimeout;
        }

        public Client setSocketTimeout(int socketTimeout) {
            this.socketTimeout = socketTimeout;
            return this;
        }

        public int getConnectionTimeout() {
            return connectionTimeout;
        }

        public Client setConnectionTimeout(int connectionTimeout) {
            this.connectionTimeout = connectionTimeout;
            return this;
        }

        public int getConnectionRequestTimeout() {
            return connectionRequestTimeout;
        }

        public Client setConnectionRequestTimeout(int connectionRequestTimeout) {
            this.connectionRequestTimeout = connectionRequestTimeout;
            return this;
        }

        public long getIdleConnectionTime() {
            return idleConnectionTime;
        }

        public Client setIdleConnectionTime(long idleConnectionTime) {
            this.idleConnectionTime = idleConnectionTime;
            return this;
        }

        public int getMaxErrorRetry() {
            return maxErrorRetry;
        }

        public Client setMaxErrorRetry(int maxErrorRetry) {
            this.maxErrorRetry = maxErrorRetry;
            return this;
        }

        public boolean isSupportCname() {
            return supportCname;
        }

        public Client setSupportCname(boolean supportCname) {
            this.supportCname = supportCname;
            return this;
        }

        public boolean isSldEnabled() {
            return sldEnabled;
        }

        public Client setSldEnabled(boolean sldEnabled) {
            this.sldEnabled = sldEnabled;
            return this;
        }

        public String getProtocol() {
            return protocol;
        }

        public Client setProtocol(String protocol) {
            this.protocol = protocol;
            return this;
        }

        public String getUserAgent() {
            return userAgent;
        }

        public Client setUserAgent(String userAgent) {
            this.userAgent = userAgent;
            return this;
        }
    }
}
