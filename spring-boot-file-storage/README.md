# spring-boot-file-storage

> 本 demo 演示了 Spring Boot 如何实现本地文件上传以及如何上传文件至七牛云平台。前端使用 vue 和 iview 实现上传页面。

## 参考

1. Spring 官方文档：https://docs.spring.io/spring-boot/docs/2.3.0.RELEASE/reference/htmlsingle/#howto-multipart-file-upload-configuration
2. 七牛云官方文档：https://developer.qiniu.com/kodo/sdk/1239/java#5