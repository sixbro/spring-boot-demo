package com.sixbro.code.gen.common;

/**
 * <p>
 * 统一状态码接口
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2022/1/16 10:30
 */
public interface IResultCode {
    /**
     * 获取状态码
     *
     * @return 状态码
     */
    Integer getCode();

    /**
     * 获取返回消息
     *
     * @return 返回消息
     */
    String getMessage();
}
