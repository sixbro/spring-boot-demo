package com.sixbro.code.gen.constants;

/**
 * <p>
 * 常量池
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2022/1/16 10:30
 */
public interface GenConstants {
    /**
     * 签名
     */
    String SIGNATURE = "xkcoding代码生成";
}
