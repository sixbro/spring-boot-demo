package com.sixbro.code.gen.common;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 统一API对象返回
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2022/1/16 10:30
 */
@Data
@NoArgsConstructor
public class ApiResult<T> {
    /**
     * 状态码
     */
    private Integer code;

    /**
     * 返回消息
     */
    private String message;

    /**
     * 状态
     */
    private boolean status;

    /**
     * 返回数据
     */
    private T data;

    public ApiResult(Integer code, String message, boolean status, T data) {
        this.code = code;
        this.message = message;
        this.status = status;
        this.data = data;
    }

    public ApiResult(IResultCode resultCode, boolean status, T data) {
        this.code = resultCode.getCode();
        this.message = resultCode.getMessage();
        this.status = status;
        this.data = data;
    }

    public ApiResult(IResultCode resultCode, boolean status) {
        this.code = resultCode.getCode();
        this.message = resultCode.getMessage();
        this.status = status;
        this.data = null;
    }

    public static <T> ApiResult success() {
        return new ApiResult<>(ApiResultCode.OK, true);
    }

    public static <T> ApiResult message(String message) {
        return new ApiResult<>(ApiResultCode.OK.getCode(), message, true, null);
    }

    public static <T> ApiResult success(T data) {
        return new ApiResult<>(ApiResultCode.OK, true, data);
    }

    public static <T> ApiResult fail() {
        return new ApiResult<>(ApiResultCode.ERROR, false);
    }

    public static <T> ApiResult fail(IResultCode resultCode) {
        return new ApiResult<>(resultCode, false);
    }

    public static <T> ApiResult fail(Integer code, String message) {
        return new ApiResult<>(code, message, false, null);
    }

    public static <T> ApiResult fail(IResultCode resultCode, T data) {
        return new ApiResult<>(resultCode, false, data);
    }

    public static <T> ApiResult fail(Integer code, String message, T data) {
        return new ApiResult<>(code, message, false, data);
    }

}
