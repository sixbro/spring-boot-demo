package com.sixbro.code.gen.common;

import lombok.Getter;

/**
 * <p>
 * 通用状态枚举
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2022/1/16 10:30
 */
@Getter
public enum ApiResultCode implements IResultCode {
    /**
     * 成功
     */
    OK(200, "成功"),
    /**
     * 失败
     */
    ERROR(500, "失败");

    /**
     * 返回码
     */
    private Integer code;

    /**
     * 返回消息
     */
    private String message;

    ApiResultCode(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

}
