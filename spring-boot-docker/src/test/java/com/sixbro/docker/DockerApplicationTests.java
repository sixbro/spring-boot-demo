package com.sixbro.docker;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-17 17:28
 */
@SpringBootTest
public class DockerApplicationTests {

    @Test
    public void test() {
        List<Integer> db = Arrays.asList(1,2,3,4);
        List<Integer> es = Arrays.asList(1,2,3,5,6,7);
        es.removeAll(db);
        System.err.println(es);
    }

    @Test
    public void test01() {
        List<Employee> employees = null;
        BigDecimal bigDecimal = Optional.ofNullable(employees).filter(Objects::nonNull).map(es -> es.stream().map(employee -> employee.getFee().multiply(employee.getQuantity())).reduce(BigDecimal.ZERO, BigDecimal::add)).orElse(BigDecimal.ONE);
        //List<String> strings = employees.stream().map(Employee::getName).collect(Collectors.toList());
        System.err.println(bigDecimal);
    }

    @Test
    public void test02() {
        String name = "王老五";
        String format = StringUtils.isNotBlank(name) ? String.format("你的名字 ： %s", name) : "你的名字 ： %s";
        System.err.println(format);
    }

    @Data
    class Employee {
        private Long id;
        private String name;
        private BigDecimal fee;
        private BigDecimal quantity;
    }
}
