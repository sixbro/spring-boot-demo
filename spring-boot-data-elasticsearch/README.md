# spring-boot-data-elasticsearch

> 此 demo 主要演示了 Spring Boot 如何集成 `spring-boot-starter-data-elasticsearch` 完成对 ElasticSearch 的高级使用技巧，包括创建索引、配置映射、删除索引、增删改查基本操作、复杂查询、高级查询、聚合查询等。

## 注意

当前ElasticSearch版本为 `6.5.3`，使用 docker 运行，下面是所有步骤：

1. 下载镜像：`docker pull elasticsearch:6.5.3`

2. 运行容器：`docker run -d -p 9200:9200 -p 9300:9300 --name elasticsearch-6.5.3 elasticsearch:6.5.3`

3. 进入容器：`docker exec -it elasticsearch-6.5.3 /bin/bash`

4. 安装 ik 分词器：`./bin/elasticsearch-plugin install https://github.com/medcl/elasticsearch-analysis-ik/releases/download/v6.5.3/elasticsearch-analysis-ik-6.5.3.zip`

5. 修改 es 配置文件：`vi ./config/elasticsearch.yml

   ```yaml
   cluster.name: "docker-cluster"
   network.host: 0.0.0.0

   # minimum_master_nodes need to be explicitly set when bound on a public IP
   # set to 1 to allow single node clusters
   # Details: https://github.com/elastic/elasticsearch/pull/17288
   discovery.zen.minimum_master_nodes: 1

   # just for elasticsearch-head plugin
   http.cors.enabled: true
   http.cors.allow-origin: "*"
   ```

6. 退出容器：`exit`

7. 停止容器：`docker stop elasticsearch-6.5.3`

8. 启动容器：`docker start elasticsearch-6.5.3`

## 注解介绍
`@Document` 注解主要声明索引名、类型名、分片数量和备份数量

`@Field` 注解主要声明字段对应ES的类型

### Spring Data 通过注解来声明字段的映射属性，有下面的三个注解：

* `@Document` 作用在类，标记实体类为文档对象，一般有四个属性
  * `indexName`：对应索引库名称
  * `type`：对应在索引库中的类型
  * `shards`：分片数量，默认5
  * `replicas`：副本数量，默认1
* `@Id` 作用在成员变量，标记一个字段作为id主键
* `@Field` 作用在成员变量，标记为文档的字段，并指定字段映射属性：
  * `type`：字段类型，取值是枚举：FieldType
  * `index`：是否索引，布尔类型，默认是true
  * `store`：是否存储，布尔类型，默认是false
  * `analyzer`：分词器名称：ik_max_word 1

## 参考

1. ElasticSearch 官方文档：https://www.elastic.co/guide/en/elasticsearch/reference/6.x/getting-started.html
2. spring-data-elasticsearch 官方文档：https://docs.spring.io/spring-data/elasticsearch/docs/3.1.2.RELEASE/reference/html/

## 参考博客
1. https://blog.csdn.net/xxacker/category_9776674.html
2. https://blog.csdn.net/csdn_20150804/category_9193831.html