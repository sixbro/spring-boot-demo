package com.sixbro.data.elasticsearch.repository;

import com.sixbro.data.elasticsearch.model.Person;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  用户持久层
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-14 13:48
 */
@Repository
public interface PersonRepository extends ElasticsearchRepository<Person, Long> {

    /**
     * 根据年龄区间查询
     *
     * @param min 最小值
     * @param max 最大值
     * @return 满足条件的用户列表
     */
    List<Person> findByAgeBetween(Integer min, Integer max);
}
