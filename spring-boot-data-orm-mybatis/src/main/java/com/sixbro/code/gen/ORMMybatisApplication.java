package com.sixbro.code.gen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2022/1/16 9:37
 */
@SpringBootApplication
public class ORMMybatisApplication {

    public static void main(String[] args) {
        SpringApplication.run(ORMMybatisApplication.class, args);
    }
}
