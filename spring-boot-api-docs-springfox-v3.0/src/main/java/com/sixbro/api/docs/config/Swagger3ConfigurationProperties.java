package com.sixbro.api.docs.config;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * <p>
 *
 * </p>
 *
 * @Author: Mr.Lu
 * @Since: 2022/1/7 8:51
 */
@Getter
@Setter
@ConfigurationProperties("springfox.documentation.swagger.v3")
public class Swagger3ConfigurationProperties {
    private String apiGroupName = "default";
    /** swagger会解析的包路径 */
    private String apiBasePackage;
    /**
     * swagger会解析的url规则
     *
     * 当没有配置任何path的时候，解析/**
     */
    private String apiBasePath;

    /** 在basePath基础上需要排除的url规则 */
    private String apiExcludePath;

    private String apiAuthParamDescription = "令牌";

    /** 标题 */
    private String apiTitle = "XX项目API";
    /** 描述　*/
    private String apiDescription = "XX项目SwaggerAPI管理";
    /** 版本 */
    private String apiVersion = "1.0";
    /** 许可证 */
    private String apiLicense = "Apache 2.0";
    /** 许可证URL */
    private String apiLicenseUrl = "http://www.apache.org/licenses/LICENSE-2.0.html";
    /** 服务条款URL */
    private String apiTermsOfServiceUrl;

    //@NestedConfigurationProperty
    private Contact apiContact = new Contact();

    @Getter
    @Setter
    public static class Contact {
        /**
         * 联系人
         **/
        private String name = "逍遥";
        /**
         * 联系人email
         **/
        private String email = "sixbro@163.com";
        /**
         * 联系人url
         **/
        private String url = "https://sixbro.cn";
    }
}
