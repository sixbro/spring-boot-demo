package com.sixbro.api.docs.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.io.Serializable;

/**
 * <p>
 *  封装统一的返回数据格式
 * </p>
 *
 * @Author: Mr.Lu
 * @Since: 2022/1/7 8:54
 */
@Getter
@Setter
@Schema(description = "统计返回数据封装")
public class ApiResultBody<T> implements Serializable {
    public static final String DEFAULT_SUCCESS_MESSAGE = "请求成功";

    /**
     * 通用返回状态
     */
    @Schema(description = "业务码")
    private Integer code;

    /**
     * 通用返回信息
     */
    @Schema(description = "消息")
    private String message;

    /**
     * 通用返回数据
     */
    @Schema(description = "数据主体")
    private T data;

    public ApiResultBody() {
    }

    public ApiResultBody(Integer code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public static <T> ApiResultBody<T> ok(){
        return ok(null);
    }

    public static <T> ApiResultBody<T> ok(T body){
        return new ApiResultBody<>(0, DEFAULT_SUCCESS_MESSAGE, body);
    }

    public static <T> ApiResultBody<T> error(Integer code, String message){
        return new ApiResultBody<>(code, message, null);
    }
}
