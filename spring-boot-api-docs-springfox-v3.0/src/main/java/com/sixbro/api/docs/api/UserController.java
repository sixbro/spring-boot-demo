package com.sixbro.api.docs.api;

import com.sixbro.api.docs.model.User;
import com.sixbro.api.docs.vo.ApiResultBody;
import com.sixbro.api.docs.vo.UserVO;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  用户管理 Controller
 * </p>
 *
 * @Author: Mr.Lu
 * @Since: 2022/1/7 9:46
 */
@RestController
@RequestMapping("/api/users")
// @Tag 注解不生效，似乎是 BUG，相关 issues：https://github.com/springfox/springfox/issues/3668，因此这里使用 2.X 的注解 @Api
@Tag(name = "用户管理", description = "用户数据增删改查")
@Api(tags = "用户管理")
public class UserController {

    @Operation(summary = "查询用户列表", description = "返回所有用户数据")
    @GetMapping("")
    public ApiResultBody<List<UserVO>> getUserList(@Parameter(description = "用户名") @RequestParam(value = "username", required = false) String userName) {
        List<UserVO> result = new ArrayList<>();
        result.add(new UserVO("zhangsan", "zhangsan@lanweihong.com"));
        result.add(new UserVO("lisi", "lisi@lanweihong.com"));
        return ApiResultBody.ok(result);
    }

    @Operation(summary = "通过用户名查询用户", description = "根据用户名查询用户详细信息")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "请求成功"),
            @ApiResponse(responseCode = "404", description = "用户不存在", content = @Content)
    })
    @GetMapping("/{username}")
    public ApiResultBody<UserVO> getUserByName(@Parameter(description = "用户名", required = true) @PathVariable("username") String userName) {
        // TODO
        return ApiResultBody.ok();
    }

    @Operation(summary = "新增用户")
    @PostMapping("")
    public ApiResultBody<UserVO> addUser(@Parameter(required = true) @Valid @RequestBody User param) {
        // TODO
        return ApiResultBody.ok();
    }

    @Operation(summary = "修改用户")
    @PutMapping("")
    public ApiResultBody<UserVO> updateUser(@Parameter(description = "用户参数", required = true) @Valid @RequestBody User param) {
        // TODO
        return ApiResultBody.ok();
    }

    @Operation(summary = "删除用户")
    @DeleteMapping("/{username}")
    public ApiResultBody<UserVO> deleteUserByName(@Parameter(name = "username", in = ParameterIn.PATH, description = "用户名", required = true) @PathVariable("username") String userName) {
        // TODO
        return ApiResultBody.ok();
    }

    @Hidden
    @PostMapping("/test")
    public ApiResultBody<UserVO> testAddUser(@Valid @RequestBody User userParam) {
        // TODO
        return ApiResultBody.ok();
    }
}