package com.sixbro.api.docs.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 *
 * </p>
 *
 * @Author: Mr.Lu
 * @Since: 2022/1/7 9:28
 */
@Getter
@Setter
@Schema(description = "用户实体")
@ToString
public class UserVO {

    @Schema(name = "用户名", description = "用户名")
    private String username;

    @Schema(description = "邮箱")
    private String email;

    public UserVO() {
    }

    public UserVO(String username, String email) {
        this.username = username;
        this.email = email;
    }
}
