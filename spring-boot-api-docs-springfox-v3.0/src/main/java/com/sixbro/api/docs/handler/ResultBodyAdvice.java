//package com.sixbro.api.docs.handler;
//
//import com.sixbro.api.docs.annotation.IgnoreApiResultBody;
//import com.sixbro.api.docs.vo.ApiResultBody;
//import org.springframework.core.MethodParameter;
//import org.springframework.http.MediaType;
//import org.springframework.http.converter.HttpMessageConverter;
//import org.springframework.http.server.ServerHttpRequest;
//import org.springframework.http.server.ServerHttpResponse;
//import org.springframework.web.bind.annotation.RestControllerAdvice;
//import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;
//
///**
// * <p>
// *  返回体统一封装器
// * </p>
// *
// * @Author: Mr.Lu
// * @Since: 2022/1/5 18:09
// */
//@RestControllerAdvice
//public class ResultBodyAdvice implements ResponseBodyAdvice<Object> {
//
//    @Override
//    public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
//        return !returnType.hasMethodAnnotation(IgnoreApiResultBody.class);
//    }
//
//    @Override
//    public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType, Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
//        if (body == null) {
//            return ApiResultBody.ok();
//        }
//        if (ApiResultBody.class.isAssignableFrom(body.getClass())) {
//            return body;
//        }
//        return ApiResultBody.ok(body);
//    }
//}
