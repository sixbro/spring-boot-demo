/*
 *
 *  * Copyright 2019-2020 the original author or authors.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      https://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package com.sixbro.api.docs.model;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * <p>
 *
 * </p>
 *
 * @Author: Mr.Lu
 * @Since: 2022/1/5 18:07
 */
// @Schema 注解用在类上不生效，使用 @ApiModel 替代
@Schema(title = "书实体标题", description = "书参数实体")
//@ApiModel(description = "用户参数对象")
public class Book {

	@Schema(description = "ID")
	private long id;

	@NotBlank(message = "名称不能为空")
	@Size(min = 0, max = 20)
	@Schema(description = "密码，6-18位，包含大小写、数字及特殊字符")
	private String title;

	@NotBlank(message = "作者不能为空")
	@Size(min = 0, max = 30)
	private String author;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}
}
