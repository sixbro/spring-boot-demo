## swagger3与swagger2区别

### 1、说明
`Swagger3`是在`Swagger2`上做了大版本升级，使用方式和Swagger2大体一致，只是需要注意区别配置地方，Swagger3使用相关依赖版本如下：

* 需要Java 8
* 需要Spring5.x（未在早期版本中测试）
* 需要SpringBoot 2.2+（未在早期版本中测试）

### 2、区别

|        区别        |                   	swagger2                   |            swagger3             |
|:----------------:|:---------------------------------------------:|:-------------------------------:|
|       依赖包        | `springfox-swagger2`、`springfox-swagger-ui`	  |    `springfox-boot-starter`     |
|       启用方式       |               `@EnableSwagger2`               |        `@EnableOpenApi`         |
|       访问方式       |            ip:port/swagger-ui.html            |  ip:port/swagger-ui/index.html  |
|   Doucument类型    |         `DocumentationType.SWAGGER_2`         |   `DocumentationType.OAS_30`    |

## 

> 原因： 这是因为Springfox使用的路径匹配是基于AntPathMatcher的，而Spring Boot 2.6.X使用的是PathPatternMatcher。  
> 解决：在application.properties里配置：spring.mvc.pathmatch.matching-strategy=ANT_PATH_MATCHER


## 参考
1. swagger 官方网站：https://swagger.io/
2. swagger 官方文档：https://github.com/swagger-api/swagger-core/wiki/Swagger-2.X---Getting-started
3. swagger 常用注解：https://github.com/swagger-api/swagger-core/wiki/Swagger-2.X---Annotations
4. springfox 官方网站：http://springfox.github.io/springfox/
5. springfox 官方文档：https://springfox.github.io/springfox/docs/current/
6. springfox 官方案例：https://github.com/springfox/springfox-demos