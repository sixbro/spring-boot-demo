## spring-boot-data-orm-jpa

> 此 demo 主要演示了 Spring Boot 如何使用 JPA 操作数据库，包含简单使用以及级联使用。

## SpringBoot整合spring-data-jpa

`jpa` 是JavaEE定义的一种规范，常用的实现一般是 `Hibernate`，而 `spring-data-jpa` 则是对`jpa`的又一层封装，提供了更多便捷的方法。

## 
JPA如何根据前台传的多个参数(有的有值，有的没有值)，进行like、equals等复杂，动态查询数据

主要使用以下几个类
* `Specification`
* `Predicate`
* `Root`
* `CriteriaQuery`
* `CriteriaBuilder`
* `PagingAndSortingRepository`
* `JpaSpecificationExecutor`

### 1.代码
#### 1-1.entity
```java
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@Data
public abstract class AbstractAuditModel implements Serializable {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, columnDefinition = "bigint(20) comment '用户的主键'")
    private Long id;

    /**
     * 创建时间
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_time", nullable = false, updatable = false)
    @CreatedDate
    private Date createTime;

    /**
     * 上次更新时间
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_update_time", nullable = false)
    @LastModifiedDate
    private Date lastUpdateTime;
}
```
```java
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Entity
@Table(name = "t_permission")
public class Permission extends AbstractAuditModel {

    @Column(nullable = false, columnDefinition = "varchar(255) comment '权限的名称'")
    private String name;

    @Column(nullable = false, columnDefinition = "varchar(255) comment '权限的描述'")
    private String description;

    @Column(nullable = false, columnDefinition = "varchar(255) comment '权限码'")
    private String code;
}
```

#### 1-2.repository
```java
@Repository
public interface PermissionRepository extends JpaRepository<Permission, Long>, JpaSpecificationExecutor<Permission> {
}
```
> **注意：这必须要继承类PagingAndSortingRepository和JpaSpecificationExecutor**

1-3.service
```java
@Service
public class PermissionService {

    private final PermissionRepository permissionRepository;

    public PermissionService(PermissionRepository permissionRepository) {
        this.permissionRepository = permissionRepository;
    }

    public Page<Permission> findPermissionByPage(int page, int size, String name, String code){
        Specification<Permission> specificationQuery = new Specification<>() {
            @Override
            public Predicate toPredicate(Root<Permission> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                List<Predicate> list = new ArrayList<>();
                if (StringUtils.isNoneBlank(name)) {
                    Path<String> pname = root.get("name");
                    Predicate predicate = criteriaBuilder.like(pname, "%" + name + "%");
                    list.add(criteriaBuilder.and(predicate));
                }

                if (StringUtils.isNoneBlank(code)) {
                    Path<String> pcode = root.get("code");
                    Predicate predicate = criteriaBuilder.equal(pcode, code);
                    list.add(criteriaBuilder.and(predicate));
                }
                Predicate[] p = new Predicate[list.size()];
                return criteriaBuilder.and(list.toArray(p));
            }
        };
        Page<Permission> permissions = permissionRepository.findAll(specificationQuery, PageRequest.of(page - 1, size));
        return permissions;
    }
}
```

### 2.方法介绍
#### 2-1.toPredicate
```java
public Predicate toPredicate(Root<DevWarnPo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder);
```

* `CriteriaQuery<?> criteriaQuery`
> 这是一个面向对象查询，代表的是Specific的顶层查询对象，  
> 它包含查询的各个部分：select，from，where，group by, order by等，不过它是一个面向对象的查询方式，只对实体类型，嵌入式类型的Criteria查询起作用。  

* `Root<T> root`
> 代表要查询的对象，也就是实体类型，实体类型好比sql语句中的from后的表。传入实体类型后，会被CriteriaQuery的父类AbstractQuery.from将实体类型传入

* `CriterBuilder criteriaBuilder`
> 用来构建CriteriaQuery的构建器对象Predicate（谓语），即：一个简单或者复杂的谓语类型，相当于条件或者多条件集合。

* `Predicate`
> 就是多条件查询中的条件，可以通过List 实现多个条件操作。