package com.sixbro.orm.jpa.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sixbro.orm.jpa.entity.base.AbstractAuditModel;
import lombok.*;

import javax.persistence.*;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021/12/10 11:11
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Entity
@Table(name = "t_user_receiving_address")
public class UserReceivingAddress extends AbstractAuditModel {

    @Column(name = "city", nullable = false)
    private String city;

    @Column(name = "address", nullable = false)
    private String address;

    /**
     * 每个用户有自己的角色,多个用户可以有同一个角色
     * CascadeType见 https://www.jianshu.com/p/e8caafce5445
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "creator", referencedColumnName = "id")
    @JsonIgnore
    private User creator;

//    @ManyToOne(targetEntity = User.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//    @JoinColumn(name = "user_id", referencedColumnName = "id",insertable = false, updatable = false)
//    @OneToOne(targetEntity = User.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//    @JoinColumn(name = "user_id", referencedColumnName = "id",insertable = false, updatable = false)
//    private User user;

//    @OneToOne(targetEntity = User.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//    @JoinColumn(name = "creator", referencedColumnName = "id",insertable = false, updatable = false)
//    private User creator;
}
