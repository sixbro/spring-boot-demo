package com.sixbro.orm.jpa.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sixbro.orm.jpa.entity.base.AbstractAuditModel;
import lombok.*;

import javax.persistence.*;
import java.util.Set;

/**
 * <p>
 *  角色
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021/12/11 11:51
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Entity
@Table(name = "t_role")
public class Role extends AbstractAuditModel {

    @Column(nullable = false, unique = true, columnDefinition = "varchar(255) comment '角色的名称'")
    private String name;

    // 一个角色可以被多个用户使用
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "role")
    @JsonIgnore
    private Set<User> users;

    // 角色和权限之间的关系是多对多
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "t_role_permission",
            joinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "permission_id", referencedColumnName = "id"))
    @JsonIgnore
    private Set<Permission> permissions;
}
