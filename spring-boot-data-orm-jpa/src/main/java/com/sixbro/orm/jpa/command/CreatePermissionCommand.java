package com.sixbro.orm.jpa.command;

import lombok.Data;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-15 9:14
 */
@Data
public class CreatePermissionCommand {

    private String name;

    private String description;

    private String code;
}
