package com.sixbro.orm.jpa.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author: Mr.Lu
 * @since: 2021/12/10 16:18
 */
@Table(name = "sys_employee")
public class Employee {

    /**
     * 主键
     */
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid.hex")
    @Column(name = "id", nullable = false, length = 64)
    private String id;

    /**
     * 名字
     */
    @Column(name = "name", length = 25)
    private String name;

    /**
     * 年龄
     */
    @Column(name = "age", length = 2)
    private Integer age;

    /**
     * 备注
     */
    @Column(name = "brief", length = 255)
    private String Brief;

    /**
     * 性别
     */
    @Column(name = "sex", length = 1)
    private Boolean sex;

}
