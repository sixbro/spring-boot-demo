package com.sixbro.orm.jpa.entity;

import com.sixbro.orm.jpa.entity.base.AbstractAuditModel;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * <p>
 *  权限，每种角色都有对应可执行操作的权限，未完成...
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021/12/11 11:54
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Entity
@Table(name = "t_permission")
public class Permission extends AbstractAuditModel {

    @Column(nullable = false, columnDefinition = "varchar(255) comment '权限的名称'")
    private String name;

    @Column(nullable = false, columnDefinition = "varchar(255) comment '权限的描述'")
    private String description;

    @Column(nullable = false, columnDefinition = "varchar(255) comment '权限码'")
    private String code;
}
