package com.sixbro.orm.jpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021/12/10 15:44
 */
@SpringBootApplication
public class ORMJPAApplication {

    public static void main(String[] args) throws URISyntaxException, IOException {
        SpringApplication.run(ORMJPAApplication.class, args);
        System.setProperty("java.awt.headless", "false");
        // 自动打开浏览器
        Desktop.getDesktop().browse(new URI("http://localhost:8080?nickname=t&city=广州"));
    }
}
