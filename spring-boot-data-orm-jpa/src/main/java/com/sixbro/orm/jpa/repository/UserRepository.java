package com.sixbro.orm.jpa.repository;

import com.sixbro.orm.jpa.entity.User;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021/12/10 17:10
 */
@Repository
public interface UserRepository extends JpaRepositoryImplementation<User, Long> {
}
