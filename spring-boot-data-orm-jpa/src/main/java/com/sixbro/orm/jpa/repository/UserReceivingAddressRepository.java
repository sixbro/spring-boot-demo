package com.sixbro.orm.jpa.repository;

import com.sixbro.orm.jpa.entity.UserReceivingAddress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021/12/10 17:10
 */
@Repository
public interface UserReceivingAddressRepository extends JpaRepository<UserReceivingAddress, Long> {
}
