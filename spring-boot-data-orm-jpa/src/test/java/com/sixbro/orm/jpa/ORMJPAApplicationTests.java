package com.sixbro.orm.jpa;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.IdUtil;
import cn.hutool.crypto.SecureUtil;
import com.google.common.collect.Lists;
import com.sixbro.orm.jpa.entity.User;
import com.sixbro.orm.jpa.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021/12/10 17:15
 */
@Slf4j
@SpringBootTest
public class ORMJPAApplicationTests {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void contextLoads() {
    }

    /**
     * 测试保存
     */
    @Test
    public void testSave() {
        String salt = IdUtil.fastSimpleUUID();
        User testSave3 = User.builder().username("testSave3").password(SecureUtil.md5("123456" + salt)).salt(salt).email("testSave3@xkcoding.com").state(User.State.NORMAL).build();
        userRepository.save(testSave3);

        Assert.notNull(testSave3.getId());
        Optional<User> byId = userRepository.findById(testSave3.getId());
        Assert.isTrue(byId.isPresent());
        log.debug("【byId】= {}", byId.get());
    }

    /**
     * 测试删除
     */
    @Test
    public void testDelete() {
        long count = userRepository.count();
        userRepository.deleteById(1L);
        long left = userRepository.count();
        Assert.isTrue(count - 1 == left);
    }

    /**
     * 测试修改
     */
    @Test
    public void testUpdate() {
        userRepository.findById(1L).ifPresent(user -> {
            user.setUsername("JPA修改名字");
            userRepository.save(user);
        });
        Assert.isTrue(userRepository.findById(1L).get().getUsername().equals("JPA修改名字"));
    }

    /**
     * 测试查询单个
     */
    @Test
    public void testQueryOne() {
        Optional<User> byId = userRepository.findById(1L);
        Assert.isTrue(byId.isPresent());
        log.debug("【byId】= {}", byId.get());
    }

    /**
     * 测试查询所有
     */
    @Test
    public void testQueryAll() {
        List<User> users = userRepository.findAll();
        Assert.isTrue(0 != users.size());
        log.debug("【users】= {}", users);
    }

    /**
     * 测试分页排序查询
     */
    @Test
    public void testQueryPage() {
        // 初始化数据
        initData();
        // JPA分页的时候起始页是页码减1
        Integer currentPage = 0;
        Integer pageSize = 5;
        Sort sort = Sort.by(Sort.Direction.DESC, "id");
        PageRequest pageRequest = PageRequest.of(currentPage, pageSize, sort);
        Page<User> userPage = userRepository.findAll(pageRequest);

        Assert.isTrue(5 == userPage.getSize());
        Assert.isTrue(userRepository.count() == userPage.getTotalElements());
        log.debug("【id】= {}", userPage.getContent().stream().map(User::getId).collect(Collectors.toList()));
    }

    @Test
    public void testIn(String organizationId){
        Specification<User> specification = new Specification<>() {
            @Override
            public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                List<Predicate> predicates = new ArrayList<>();

                if (StringUtils.isNoneBlank(organizationId)) {
                    Predicate predicate = builder.equal(root.get("organization").get("id"), organizationId);
                    predicates.add(builder.and(predicate));
                }

                // in
                predicates.add(builder.in(root.get("state")).in(User.State.DELETED, User.State.DISABLED));

                return query.where(predicates.toArray(new Predicate[predicates.size()])).getRestriction();
            }
        };
    }

    @Test
    public void testNotIn(String city){
        Specification<User> specification = new Specification<>() {
            @Override
            public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                List<Predicate> predicates = new ArrayList<>();

                if (StringUtils.isNoneBlank(city)) {
                    Predicate predicate = builder.equal(root.get("receivingAddresses").get("city"), city);
                    predicates.add(builder.and(predicate));
                }

                // not in
                predicates.add(builder.not(root.get("state")).in(User.State.DELETED, User.State.DISABLED));

                return query.where(predicates.toArray(new Predicate[predicates.size()])).getRestriction();
            }
        };
    }

    /**
     * 初始化10条数据
     */
    private void initData() {
        List<User> userList = Lists.newArrayList();
        for (int i = 0; i < 10; i++) {
            String salt = IdUtil.fastSimpleUUID();
            int index = 3 + i;
            User user = User.builder().username("testSave" + index).password(SecureUtil.md5("123456" + salt)).salt(salt).email("testSave" + index + "@xkcoding.com").state(User.State.NORMAL).build();
            userList.add(user);
        }
        userRepository.saveAll(userList);
    }
}
