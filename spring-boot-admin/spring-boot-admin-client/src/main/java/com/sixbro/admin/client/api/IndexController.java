package com.sixbro.admin.client.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  首页
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-15 18:11
 */
@RestController
public class IndexController {

    @GetMapping(value = {"", "/"})
    public String index() {
        return "This is a Spring Boot Admin Client.";
    }
}