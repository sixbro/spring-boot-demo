package com.sixbro.data.elasticsearch;

import com.sixbro.data.elasticsearch.model.Conference;
import com.sixbro.data.elasticsearch.model.Person;
import com.sixbro.data.elasticsearch.repository.ConferenceRepository;
import com.sixbro.data.elasticsearch.service.PersonService;
import com.sixbro.data.elasticsearch.util.UUIDGenerator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-14 14:53
 */
@SpringBootApplication
public class DataElasticsearchRestHighLevelClientApplicationTests {

    @Autowired
    private ElasticsearchOperations operations;
    @Autowired
    private ConferenceRepository conferenceRepository;
    @Autowired
    private PersonService personService;

    /**
     * 测试 ElasticTemplate 的创建/删除
     */
    @Test
    public void createIndex() {
        operations.indexOps(Conference.class).refresh();

        // Save data sample

        var documents = Arrays.asList(
                Conference.builder().date("2014-11-06").name("Spring eXchange 2014 - London")
                        .keywords(Arrays.asList("java", "spring")).location(new GeoPoint(51.500152D, -0.126236D))
                        .build(),
                Conference.builder().date("2014-12-07").name("Scala eXchange 2014 - London")
                        .keywords(Arrays.asList("scala", "play", "java")).location(new GeoPoint(51.500152D, -0.126236D))
                        .build(),
                Conference.builder().date("2014-11-20").name("Elasticsearch 2014 - Berlin")
                        .keywords(Arrays.asList("java", "elasticsearch", "kibana")).location(new GeoPoint(52.5234051D, 13.4113999))
                        .build(),
                Conference.builder().date("2014-11-12").name("AWS London 2014").keywords(Arrays.asList("cloud", "aws"))
                        .location(new GeoPoint(51.500152D, -0.126236D))
                        .build(),
                Conference.builder().date("2014-10-04").name("JDD14 - Cracow").keywords(Arrays.asList("java", "spring"))
                        .location(new GeoPoint(50.0646501D, 19.9449799))
                        .build()
        );

        conferenceRepository.saveAll(documents);
    }

    @Test
    public void deleteIndex() {
        operations.indexOps(Conference.class).delete();
    }

    @Test
    public void search() {
        conferenceRepository.searchSimilar(Conference.builder().id(UUIDGenerator.newUUIDNumeric()).build(), new String[]{""}, PageRequest.of(0, 2));
    }

    /**
     * 测试创建索引
     */
    @Test
    public void createIndexTest() {
        personService.createIndex(Person.INDEX_NAME);
    }

    /**
     * 测试删除索引
     */
    @Test
    public void deleteIndexTest() {
        personService.deleteIndex(Person.INDEX_NAME);
    }

    /**
     * 测试新增
     */
    @Test
    public void insertTest() {
        List<Person> list = new ArrayList<>();
        list.add(Person.builder().age(11).birthday(new Date()).country("CN").id(1L).name("哈哈").remark("test1").build());
        list.add(Person.builder().age(22).birthday(new Date()).country("US").id(2L).name("hiahia").remark("test2").build());
        list.add(Person.builder().age(33).birthday(new Date()).country("ID").id(3L).name("呵呵").remark("test3").build());

        personService.insert(Person.INDEX_NAME, list);
    }

    /**
     * 测试更新
     */
    @Test
    public void updateTest() {
        Person person = Person.builder().age(33).birthday(new Date()).country("ID_update").id(3L).name("呵呵update").remark("test3_update").build();
        List<Person> list = new ArrayList<>();
        list.add(person);
        personService.update(Person.INDEX_NAME, list);
    }

    /**
     * 测试删除
     */
    @Test
    public void deleteTest() {
        personService.delete(Person.INDEX_NAME, Person.builder().id(1L).build());
    }

    /**
     * 测试查询
     */
    @Test
    public void searchListTest() {
        List<Person> personList = personService.searchList(Person.INDEX_NAME);
        System.out.println(personList);
    }
}
