package com.sixbro.data.elasticsearch.common;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-14 17:08
 */
public class PageQuery implements Serializable {
    private static final long serialVersionUID = 7172912761241281958L;

    /**
     * 当前页
     */
    private Integer page = 0;
    /**
     * 条目数
     */
    private Integer size = 20;

    /**
     * 关键字
     */
    private String keyword;

    /** 排序字段 */
    private String sortField;

    /** 排序方式 asc,desc */
    private String sortWay;

    public Integer getPage() {
        return page;
    }

    public PageQuery setPage(Integer page) {
        this.page = page;
        return this;
    }

    public Integer getSize() {
        return size;
    }

    public PageQuery setSize(Integer size) {
        this.size = size;
        return this;
    }

    public String getKeyword() {
        return keyword;
    }

    public PageQuery setKeyword(String keyword) {
        this.keyword = keyword;
        return this;
    }

    public String getSortField() {
        return sortField;
    }

    public PageQuery setSortField(String sortField) {
        this.sortField = sortField;
        return this;
    }

    public String getSortWay() {
        return sortWay;
    }

    public PageQuery setSortWay(String sortWay) {
        this.sortWay = sortWay;
        return this;
    }
}
