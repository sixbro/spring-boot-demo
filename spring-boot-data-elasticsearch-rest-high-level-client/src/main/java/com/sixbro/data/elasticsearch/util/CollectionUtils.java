package com.sixbro.data.elasticsearch.util;

import cn.hutool.core.collection.CollUtil;

import java.util.*;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-14 17:15
 */
public class CollectionUtils extends org.springframework.util.CollectionUtils {

    public static boolean isNotEmpty(Collection<?> collection){
        return !isEmpty(collection);
    }

    public static boolean isNotEmpty(Map<?, ?> map){
        return !isEmpty(map);
    }

    public static <T> List<T> singleList(T value){
        return CollUtil.newArrayList(value);
    }
}
