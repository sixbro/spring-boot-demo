package com.sixbro.data.elasticsearch.exception;

import com.sixbro.data.elasticsearch.common.ApiResultCode;
import lombok.Getter;

/**
 * <p>
 *  ElasticsearchException
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-14 16:23
 */
public class ElasticsearchException extends RuntimeException {

    @Getter
    private int code;

    @Getter
    private String message;

    public ElasticsearchException(String message) {
        super(message);
    }

    public ElasticsearchException(int code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }

    public ElasticsearchException(ApiResultCode apiResultCode) {
        this(apiResultCode.getCode(), apiResultCode.getMessage());
    }

    public ElasticsearchException(String message, Throwable cause) {
        super(message, cause);
    }

    public ElasticsearchException(Throwable cause) {
        super(cause);
    }

    public ElasticsearchException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
