package com.sixbro.data.elasticsearch.common;

import org.elasticsearch.search.sort.SortOrder;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-14 17:10
 */
public class SortParam implements Serializable {
    private static final long serialVersionUID = -379151600753725891L;

    /** 排序字段 */
    private String fieldName;
    /** 排序方式 */
    private SortOrder order;

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public SortOrder getOrder() {
        return order;
    }

    public void setOrder(SortOrder order) {
        this.order = order;
    }
}
