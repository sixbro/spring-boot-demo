package com.sixbro.data.elasticsearch.util;

import java.nio.ByteBuffer;
import java.time.LocalDateTime;
import java.util.Base64;
import java.util.UUID;
import java.util.regex.Pattern;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-14 17:44
 */
public class UUIDGenerator {
    private static final Base64.Encoder encoder = Base64.getUrlEncoder( );

    public static String newBase64UUID( ) {
        UUID uuid = UUID.randomUUID( );
        byte[] src = ByteBuffer.wrap( new byte[ 16 ] )
                .putLong( uuid.getMostSignificantBits( ) )
                .putLong( uuid.getLeastSignificantBits( ) )
                .array( );
        return encoder.encodeToString( src ).substring( 0, 22 );
    }

    public static String newUUID( ) {
        return UUID.randomUUID( ).toString( ).replace( "-", "" );
    }

    public static String newUUIDUpperCase( ) {
        return newUUID( ).toUpperCase( );
    }

    public static String newUUIDNumeric( ) {
        return Pattern.compile( "[^0-9]" ).matcher( newUUID( ) ).replaceAll( "" ).trim( );
    }
}
