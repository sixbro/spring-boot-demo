package com.sixbro.data.elasticsearch.repository;

import com.sixbro.data.elasticsearch.model.Conference;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-14 13:41
 */
@Repository
public interface ConferenceRepository extends ElasticsearchRepository<Conference, String> {
}
