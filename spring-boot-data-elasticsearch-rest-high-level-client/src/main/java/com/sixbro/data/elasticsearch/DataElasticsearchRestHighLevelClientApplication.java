package com.sixbro.data.elasticsearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-14 11:41
 */
@SpringBootApplication
public class DataElasticsearchRestHighLevelClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(DataElasticsearchRestHighLevelClientApplication.class, args);
    }
}
