package com.sixbro.data.elasticsearch.common;

import lombok.Data;
import org.springframework.lang.Nullable;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-14 16:24
 */
@Data
public class ApiResult<T> implements Serializable {
    private static final long serialVersionUID = 1696194043024336235L;

    /**
     * 错误码
     */
    private int code;

    /**
     * 错误信息
     */
    private String message;

    /**
     * 响应数据
     */
    private T data;

    public ApiResult() {
    }

    private ApiResult(int code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    private ApiResult(int code, String message) {
        this(code, message, null);
    }

    private ApiResult(ApiResultCode apiResultCode, T data) {
        this(apiResultCode.getCode(), apiResultCode.getMessage(), data);
    }

    private ApiResult(ApiResultCode apiResultCode) {
        this(apiResultCode.getCode(), apiResultCode.getMessage());
    }

    /**
     * 返回成功
     *
     * @param <T>   泛型标记
     * @return      响应信息 {@code ApiResult}
     */
    public static <T> ApiResult<T> success() {
        return new ApiResult<>(ApiResultCode.SUCCESS);
    }

    /**
     * 返回成功-携带数据
     *
     * @param data  响应数据
     * @param <T>   泛型标记
     * @return      响应信息 {@code ApiResult}
     */
    public static <T> ApiResult<T> success(@Nullable T data) {
        return new ApiResult<>(ApiResultCode.SUCCESS, data);
    }
}
