# spring-boot-samples-orm-mybatis-mapper-page

> 此 demo 演示了 Spring Boot 如何集成通用Mapper插件和分页助手插件，简化Mybatis开发，带给你难以置信的开发体验。

## 参考

- 通用Mapper 官方文档：https://github.com/abel533/Mapper/wiki/1.integration
- pagehelper 官方文档：https://github.com/pagehelper/Mybatis-PageHelper/blob/master/wikis/zh/HowToUse.md