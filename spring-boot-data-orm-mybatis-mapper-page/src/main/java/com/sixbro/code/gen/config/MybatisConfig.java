package com.sixbro.code.gen.config;

import org.springframework.context.annotation.Configuration;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2022/1/16 9:49
 */
@Configuration
@MapperScan(basePackages = {"com.sixbro.orm.mybatis.mapper"})
public class MybatisConfig {
}
