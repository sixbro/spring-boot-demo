package com.sixbro.code.gen.mapper;

import com.sixbro.code.gen.entity.User;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * <p>
 * UserMapper
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2022/1/16 9:48
 */
@Repository
public interface UserMapper extends Mapper<User>, MySqlMapper<User> {
}
