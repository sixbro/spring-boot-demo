package com.sixbro.code.gen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2022/1/16 9:48
 */
@SpringBootApplication
public class ORMMybatisMapperPageApplication {

    public static void main(String[] args) {
        SpringApplication.run(ORMMybatisMapperPageApplication.class, args);
    }
}
