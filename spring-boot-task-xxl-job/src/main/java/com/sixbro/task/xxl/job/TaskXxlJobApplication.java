package com.sixbro.task.xxl.job;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 *
 * </p>
 *
 * @Author: Mr.Lu
 * @Since: 2022/1/4 17:53
 */
@SpringBootApplication
public class TaskXxlJobApplication {

    public static void main(String[] args) {
        SpringApplication.run(TaskXxlJobApplication.class, args);
    }
}
