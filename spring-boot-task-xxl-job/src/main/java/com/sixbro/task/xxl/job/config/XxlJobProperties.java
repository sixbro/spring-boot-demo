package com.sixbro.task.xxl.job.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * <p>
 * xxl-job 配置
 * </p>
 *
 * @Author: Mr.Lu
 * @Since: 2020/6/23 13:19
 */
@Data
@ConfigurationProperties(prefix = "xxl.job")
public class XxlJobProperties {
    /**
     * 调度中心配置
     */
    private XxlJobAdminProperties admin;

    /**
     * 执行器配置
     */
    private XxlJobExecutorProperties executor;

    /**
     * 与调度中心交互的accessToken
     */
    private String accessToken;

    @Data
    public static class XxlJobAdminProperties {
        /**
         * 调度中心地址
         */
        private String address;
    }

    @Data
    public static class XxlJobExecutorProperties {
        /**
         * 执行器名称
         */
        private String appName;

        /**
         * 执行器 IP
         */
        private String ip;

        /**
         * 执行器端口
         */
        private int port;

        /**
         * 执行器日志
         */
        private String logPath;

        /**
         * 执行器日志保留天数，-1
         */
        private int logRetentionDays;
    }
}
