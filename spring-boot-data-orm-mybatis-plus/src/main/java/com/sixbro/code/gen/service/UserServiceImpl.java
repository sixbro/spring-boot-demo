package com.sixbro.code.gen.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sixbro.code.gen.entity.User;
import com.sixbro.code.gen.mapper.UserMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * User Service
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2022/1/16 10:06
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
}
