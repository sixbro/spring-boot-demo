package com.sixbro.code.gen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sixbro.code.gen.entity.User;
import org.springframework.stereotype.Component;

/**
 * <p>
 * UserMapper
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2022/1/16 10:06
 */
@Component
public interface UserMapper extends BaseMapper<User> {
}
