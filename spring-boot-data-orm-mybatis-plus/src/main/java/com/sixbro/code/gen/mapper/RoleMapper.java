package com.sixbro.code.gen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sixbro.code.gen.entity.Role;

/**
 * <p>
 * RoleMapper
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2022/1/16 10:06
 */
public interface RoleMapper extends BaseMapper<Role> {
}
