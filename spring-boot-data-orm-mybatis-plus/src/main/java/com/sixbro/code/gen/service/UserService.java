package com.sixbro.code.gen.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sixbro.code.gen.entity.User;

/**
 * <p>
 * User Service
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2022/1/16 10:06
 */
public interface UserService extends IService<User> {
}
