package com.sixbro.code.gen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2022/1/16 10:06
 */
@SpringBootApplication
public class ORMMybatisPlusApplication {

    public static void main(String[] args) {
        SpringApplication.run(ORMMybatisPlusApplication.class, args);
    }
}
