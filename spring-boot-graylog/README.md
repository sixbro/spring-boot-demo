# spring-boot-graylog

> 此 demo 主要演示了 Spring Boot 项目如何接入 GrayLog 进行日志管理。

## 注意

作者在编写此 demo 时，`graylog` 采用 `docker-compose` 启动，其中 `graylog` 依赖的 `mongodb` 以及 `elasticsearch` 都同步启动，生产环境建议使用外部存储。

## 1. 环境准备

**编写 `graylog` 的 `docker-compose` 启动文件**

> 如果本地没有 `mongo:3` 和 `elasticsearch-oss:6.6.1` 的镜像，会比较耗时间

```yaml
version: '2'
services:
  # MongoDB: https://hub.docker.com/_/mongo/
  mongodb:
    image: mongo:3
  # Elasticsearch: https://www.elastic.co/guide/en/elasticsearch/reference/6.6/docker.html
  elasticsearch:
    image: docker.elastic.co/elasticsearch/elasticsearch-oss:6.6.1
    environment:
      - http.host=0.0.0.0
      - transport.host=localhost
      - network.host=0.0.0.0
      - "ES_JAVA_OPTS=-Xms512m -Xmx512m"
    ulimits:
      memlock:
        soft: -1
        hard: -1
    mem_limit: 1g
  # Graylog: https://hub.docker.com/r/graylog/graylog/
  graylog:
    image: graylog/graylog:3.0
    environment:
      # 加密盐值，不设置，graylog会启动失败
      # 该字段最少需要16个字符
      - GRAYLOG_PASSWORD_SECRET=somepasswordpepper
      # 设置用户名
      - GRAYLOG_ROOT_USERNAME=admin
      # 设置密码，此为密码进过SHA256加密后的字符串
      # 加密方式，执行 echo -n "Enter Password: " && head -1 </dev/stdin | tr -d '\n' | sha256sum | cut -d" " -f1
      - GRAYLOG_ROOT_PASSWORD_SHA2=8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918
      - GRAYLOG_HTTP_EXTERNAL_URI=http://127.0.0.1:9000/
      # 设置时区
      - GRAYLOG_ROOT_TIMEZONE=Asia/Shanghai
    links:
      - mongodb:mongo
      - elasticsearch
    depends_on:
      - mongodb
      - elasticsearch
    ports:
      # Graylog web interface and REST API
      - 9000:9000
      # Syslog TCP
      - 1514:1514
      # Syslog UDP
      - 1514:1514/udp
      # GELF TCP
      - 12201:12201
      # GELF UDP
      - 12201:12201/udp
```

## 参考

- graylog 官方下载地址：https://www.graylog.org/downloads#open-source

- graylog 官方docker镜像：https://hub.docker.com/r/graylog/graylog/

- graylog 镜像启动方式：http://docs.graylog.org/en/stable/pages/installation/docker.html

- graylog 启动参数配置：http://docs.graylog.org/en/stable/pages/configuration/server.conf.html

  注意，启动参数需要加 `GRAYLOG_` 前缀

- 日志收集依赖：https://github.com/osiegmar/logback-gelf