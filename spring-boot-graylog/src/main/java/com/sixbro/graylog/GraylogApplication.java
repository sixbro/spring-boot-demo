package com.sixbro.graylog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-20 17:02
 */
@SpringBootApplication
public class GraylogApplication {

    public static void main(String[] args) {
        SpringApplication.run(GraylogApplication.class, args);
    }
}
