package com.sixbro.data.neo4j.repository;

import com.sixbro.data.neo4j.model.Lesson;
import org.springframework.data.neo4j.repository.Neo4jRepository;

/**
 * <p>
 * 课程节点Repository
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-25 11:02
 */
public interface LessonRepository extends Neo4jRepository<Lesson, String> {
}
