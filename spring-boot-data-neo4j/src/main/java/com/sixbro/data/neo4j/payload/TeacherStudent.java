package com.sixbro.data.neo4j.payload;

import com.sixbro.data.neo4j.model.Student;
import lombok.Data;

import java.util.List;

/**
 * <p>
 *  师生关系
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-25 11:01
 */
@Data
public class TeacherStudent {
    /**
     * 教师姓名
     */
    private String teacherName;

    /**
     * 学生信息
     */
    private List<Student> students;
}
