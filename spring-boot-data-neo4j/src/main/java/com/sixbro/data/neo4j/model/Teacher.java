package com.sixbro.data.neo4j.model;

import lombok.*;
import org.springframework.data.neo4j.core.schema.GeneratedValue;
import org.springframework.data.neo4j.core.schema.Id;

/**
 * <p>
 *  教师节点
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-25 10:41
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor(staticName = "of")
public class Teacher {
    /**
     * 主键，自定义主键策略，使用UUID生成
     */
    @Id
    @GeneratedValue
    private String id;

    /**
     * 教师姓名
     */
    @NonNull
    private String name;
}
