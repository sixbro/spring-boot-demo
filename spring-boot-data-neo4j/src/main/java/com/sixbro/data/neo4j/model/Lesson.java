package com.sixbro.data.neo4j.model;

import com.sixbro.data.neo4j.constants.NeoConsts;
import lombok.*;
import org.springframework.data.neo4j.core.schema.GeneratedValue;
import org.springframework.data.neo4j.core.schema.Id;
import org.springframework.data.neo4j.core.schema.Relationship;

/**
 * <p>
 *  课程节点
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-25 10:43
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor(staticName = "of")
public class Lesson {
    /**
     * 主键，自定义主键策略，使用UUID生成
     */
    @Id
    @GeneratedValue
    private String id;

    /**
     * 课程名称
     */
    @NonNull
    private String name;

    /**
     * 任教老师
     */
    @Relationship(NeoConsts.R_TEACHER_OF_LESSON)
    @NonNull
    private Teacher teacher;
}
