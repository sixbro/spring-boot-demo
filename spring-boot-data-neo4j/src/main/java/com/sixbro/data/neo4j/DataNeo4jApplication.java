package com.sixbro.data.neo4j;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-24 17:58
 */
@SpringBootApplication
public class DataNeo4jApplication {

    public static void main(String[] args) {
        SpringApplication.run(DataNeo4jApplication.class, args);
    }
}
