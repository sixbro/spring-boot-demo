package com.sixbro.data.neo4j.payload;

import com.sixbro.data.neo4j.model.Student;
import lombok.Data;

import java.util.List;

/**
 * <p>
 *  按照课程分组的同学关系
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-25 11:01
 */
@Data
public class ClassmateInfoGroupByLesson {
    /**
     * 课程名称
     */
    private String lessonName;

    /**
     * 学生信息
     */
    private List<Student> students;
}
