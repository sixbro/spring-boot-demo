package com.sixbro.data.neo4j.model;

import com.sixbro.data.neo4j.constants.NeoConsts;
import lombok.*;
import org.springframework.data.neo4j.core.schema.GeneratedValue;
import org.springframework.data.neo4j.core.schema.Id;
import org.springframework.data.neo4j.core.schema.Relationship;

/**
 * <p>
 * 班级节点
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-25 10:36
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor(staticName = "of")
public class Classes {
    /**
     * 主键
     */
    @Id
    @GeneratedValue
    private String id;

    /**
     * 班级名称
     */
    @NonNull
    private String name;

    /**
     * 班级的班主任
     */
    @Relationship(NeoConsts.R_BOSS_OF_CLASS)
    @NonNull
    private Teacher boss;
}