# spring-boot-api-docs-swagger2

>   此 demo 主要演示了 Spring Boot 如何集成原生 swagger ，自动生成 API 文档。
>   启动项目，访问地址：http://localhost:8080/swagger-ui.html 或者 http://localhost:8080/doc.html

## 常见swagger注解
### 最常用的5个注解
```text
@Api：修饰整个类，描述Controller的作用

@ApiOperation：描述一个类的一个方法，或者说一个接口

@ApiParam：单个参数描述

@ApiModel：用对象来接收参数

@ApiProperty：用对象接收参数时，描述对象的一个字段
```

###其它若干
```text
@ApiResponse：HTTP响应其中1个描述

@ApiResponses：HTTP响应整体描述

@ApiIgnore：使用该注解忽略这个API 

@ApiClass

@ApiError
@ApiErrors

@ApiParamImplicit
@ApiParamsImplicit
```
-----------------------------------

## springboot + swagger的实体类属性注解
```text
@Api：用在类上，说明该类的作用
@ApiOperation：用在方法上，说明方法的作用
@ApiImplicitParams：用在方法上包含一组参数说明
@ApiImplicitParam：用在@ApiImplicitParams注解中，指定一个请求参数的各个方面
  paramType：参数放在哪个地方
      header-->请求参数的获取：@RequestHeader
      query-->请求参数的获取：@RequestParam
      path（用于restful接口）-->请求参数的获取：@PathVariable
      body（不常用）
      form（不常用）
  name：参数名
  dataType：参数类型
  required：参数是否必须传
  value：参数的意思
  defaultValue：参数的默认值
  
@ApiResponses：用于表示一组响应
@ApiResponse：用在@ApiResponses中，一般用于表达一个错误的响应信息
  code：数字，例如400
  message：信息，例如"请求参数没填好"
  response：抛出异常的类
  
@ApiModel：描述一个Model的信息（这种一般用在post创建的时候，使用@RequestBody这样的场景，请求参数无法使用@ApiImplicitParam注解进行描述的时候）
@ApiModelProperty：描述一个model的属性
```
-----------------------------------


## 参考
1. swagger 官方网站：https://swagger.io/
2. swagger 官方文档：https://github.com/swagger-api/swagger-core/wiki/Swagger-2.X---Getting-started
3. swagger 常用注解：https://github.com/swagger-api/swagger-core/wiki/Swagger-2.X---Annotations