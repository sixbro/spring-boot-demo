package com.sixbro.api.docs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiDocsSpringfoxV2Application {

    public static void main(String[] args) {
        SpringApplication.run(ApiDocsSpringfoxV2Application.class, args);
        System.err.println("http://localhost:8080/swagger-ui.html");
        System.err.println("http://localhost:8080/doc.html");
    }
}
