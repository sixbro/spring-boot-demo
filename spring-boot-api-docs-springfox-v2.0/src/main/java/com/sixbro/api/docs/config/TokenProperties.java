package com.sixbro.api.docs.config;

import com.sixbro.api.docs.swagger2.constants.AuthorizationType;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *
 * </p>
 *
 * @Author: Mr.Lu
 * @Since: 2020/6/17 12:28
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "api-docs.token")
public class TokenProperties {

    private String secret = "giemsengsahg:wen#_" ;
    private Integer expiration = 1 ;

    private String authorizationHeader = "Authorization";
    private String authorizationType = AuthorizationType.BEARER_TOKEN.name();
    private String tokenHeader = "Bearer ";
    private static final Map<AuthorizationType,String> authorizationTypes;

    static{
        authorizationTypes = new HashMap<AuthorizationType,String>();
        authorizationTypes.put(AuthorizationType.NO_AUTH,             "Bearer ");
        authorizationTypes.put(AuthorizationType.OAUTH_1,             "Bearer ");
        authorizationTypes.put(AuthorizationType.OAUTH_2,             "Bearer ");
        authorizationTypes.put(AuthorizationType.BASIC_AUTH,          "Bearer ");
        authorizationTypes.put(AuthorizationType.DIGEST_AUTH,         "Bearer ");
        authorizationTypes.put(AuthorizationType.BEARER_TOKEN,        "Bearer ");
        authorizationTypes.put(AuthorizationType.HAWK_AUTHENTICATION, "Bearer ");
        authorizationTypes.put(AuthorizationType.AWS_SIGNATURE,       "Bearer ");
    }

    public void setAuthorizationType(AuthorizationType type) {
        this.authorizationType = type.name();
        if( type != null){
            authorizationTypes.get( authorizationType );
        }
    }

    private SpringSecurity springSecurity = new SpringSecurity();

    @Data
    @SuppressWarnings( "WeakerAccess" )
    public class SpringSecurity {

        private String[] staticIgnoreds;
        private String[] apiIgnoreds;
    }
}
