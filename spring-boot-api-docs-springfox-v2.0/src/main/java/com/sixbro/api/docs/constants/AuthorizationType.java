package com.sixbro.api.docs.constants;

/**
 * <p>
 *  Authorization 认证类型
 * </p>
 *
 * @Author: Mr.Lu
 * @Since: 2022/1/4 9:57
 */
public enum AuthorizationType {
    NO_AUTH, BEARER_TOKEN, BASIC_AUTH, DIGEST_AUTH, OAUTH_1, OAUTH_2, HAWK_AUTHENTICATION, AWS_SIGNATURE
}
