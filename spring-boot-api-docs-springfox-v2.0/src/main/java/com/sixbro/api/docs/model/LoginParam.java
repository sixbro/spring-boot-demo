package com.sixbro.api.docs.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 *  登录认证接口参数
 * </p>
 *
 * @Author: Mr.Lu
 * @Since: 2022/1/3 17:27
 */
@Data
@ApiModel(value="登录认证接口参数", description="用户登录表单对象")
public class LoginParam {

    @ApiModelProperty(value = "用户名", name = "username", required = true, example = "root")
    private String username;
    @ApiModelProperty(value = "密码", name = "password", required = true, example = "123456")
    private String password;
    @ApiModelProperty(value = "Application ID", name = "appid", required = false, example = "gw4gh3q4hg4q3gh43")
    private String appid;
    @ApiModelProperty(value = "IMEI码", name = "imei", required = false, example = "TUYIUOIU234234YUII")
    private String imei;
}
