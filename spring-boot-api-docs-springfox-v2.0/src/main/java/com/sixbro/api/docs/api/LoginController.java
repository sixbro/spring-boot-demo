package com.sixbro.api.docs.api;

import com.alibaba.fastjson.JSON;
import com.sixbro.api.docs.swagger2.model.LoginParam;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  登录接口类
 * </p>
 *
 * @Author: Mr.Lu
 * @Since: 2022/1/4 10:11
 */
@Api(value = "登录请求接口类", tags = "登录", description = "用户请求登录获取Token")
@ApiResponses(value = {
        @ApiResponse(code = 200, message = "请求已完成"),
        @ApiResponse(code = 201, message = "资源成功被创建"),
        @ApiResponse(code = 400, message = "请求中有语法问题，或不能满足请求"),
        @ApiResponse(code = 401, message = "未授权客户机访问数据"),
        @ApiResponse(code = 403, message = "服务器接受请求，但是拒绝处理"),
        @ApiResponse(code = 404, message = "服务器找不到给定的资源；文档不存在"),
        @ApiResponse(code = 500, message = "服务器出现异常")}
)
@RestController
public class LoginController {

    @ApiOperation(value = "登录认证接口", notes = "登录认证接口的说明 : 使用HTTPS协议。调用RESTful API方式", produces = "application/json")
    @PostMapping(value = "/login")
    public Object login(
            @RequestHeader(name="Content-Type", defaultValue = "application/json") String contentType,
            @ApiParam(value = "登录参数") @RequestBody LoginParam loginParam
    ){
        return JSON.parseObject(JSON.toJSONString(loginParam));
    }

    @ApiOperation(value = "登录接口2",notes = "登录接口的说明 : 使用URL query参数")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username",//参数名字
                    value = "用户名",//参数的描述
                    required = true,//是否必须传入
                    //paramType定义参数传递类型：有path,query,body,form,header
                    paramType = "query"
            )
            ,
            @ApiImplicitParam(name = "password",//参数名字
                    value = "密码",//参数的描述
                    required = true,//是否必须传入
                    paramType = "query"
            )
    })
    @PostMapping(value = "/login2")
    public LoginParam login2(String username, String password){
        System.out.println(username+":"+password);
        LoginParam loginParam = new LoginParam();
        loginParam.setUsername(username);
        loginParam.setPassword(password);
        return loginParam;
    }

    @PostMapping("/login3/{id1}/{id2}")
    @ApiOperation(value = "登录接口3",notes = "登录接口的说明 : 使用路径参数")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id1",//参数名字
                    value = "用户名",//参数的描述
                    required = true,//是否必须传入
                    //paramType定义参数传递类型：有path,query,body,form,header
                    paramType = "path"
            )
            ,
            @ApiImplicitParam(name = "id2",//参数名字
                    value = "密码",//参数的描述
                    required = true,//是否必须传入
                    paramType = "path"
            )
    })
    public String login3(@PathVariable Integer id1,@PathVariable Integer id2){
        return id1+":"+id2;
    }

    @PostMapping("/login4")
    @ApiOperation(value = "登录接口4",notes = "登录接口的说明 ：用header传递参数")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username",//参数名字
                    value = "用户名",//参数的描述
                    required = true,//是否必须传入
                    //paramType定义参数传递类型：有path,query,body,form,header
                    paramType = "header"
            )
            ,
            @ApiImplicitParam(name = "password",//参数名字
                    value = "密码",//参数的描述
                    required = true,//是否必须传入
                    paramType = "header"
            )
    })
    public String login4( @RequestHeader String username, @RequestHeader String password){
        return username + ":" + password;
    }

    @PostMapping("/login6")
    @ApiOperation(value = "带token的接口",notes = "带token的接口 ：如果需要额外的参数，非本方法用到，但过滤器要用,类似于权限token")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "authorization",//参数名字
                    value = "授权token",//参数的描述
                    required = true,//是否必须传入
                    paramType = "header"
            )
            ,
            @ApiImplicitParam(name = "username",//参数名字
                    value = "用户名",//参数的描述
                    required = true,//是否必须传入
                    paramType = "query"
            )
    })
    public String login6(String username){
        return username;
    }
}
