package com.sixbro.api.docs.config;

import com.github.xiaoymin.swaggerbootstrapui.annotations.EnableSwaggerBootstrapUI;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import org.springframework.util.StringUtils;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

// 开启swagger功能
@EnableSwagger2
// 开启SwaggerBootstrapUI
@EnableSwaggerBootstrapUI
@Configuration
@EnableConfigurationProperties(Swagger2Properties.class)
public class Swagger2Config {

    @Autowired
    private Swagger2Properties properties;

    /**
     * 配置了Swagger的docket实例
     * @return
     */
    @Bean
    public Docket docket(Environment environment) {
        //设置要显示的swagger环境
        Profiles profiles = Profiles.of("dev","test");
        //通过environment.acceptsProfiles 判断是否自己处在设定的环境
        boolean flag = environment.acceptsProfiles(profiles);

        //如果有额外的全局参数，比如说请求头参数，可以这样添加
        List<Parameter> parameters = new ArrayList<>();
        parameters.add(
                new ParameterBuilder()
                        .name("Authorization")
                        .description(properties.getApiAuthParamDescription())
                        .modelRef(new ModelRef("string"))
                        .parameterType("header")
                        .required(false)
                        .defaultValue("Bearer ")
                        .build()
        );

        // DocumentationType.SWAGGER_2 固定的，代表swagger2
        return new Docket(DocumentationType.SWAGGER_2)
                // 是否开启swagger, 默认是开启的
                .enable(Optional.ofNullable(properties.getEnabled()).orElse(flag))
                .host("localhost")
//                .produces(Sets.newHashSet("application/json"))
//                .consumes(Sets.newHashSet("application/json"))
//                .protocols(Sets.newHashSet("http", "https"))
                // 如果配置多个文档的时候，那么需要配置groupName来分组标识
                //.groupName("分布式任务系统")
                // 用于生成API信息
                .apiInfo(apiInfo())
//                .genericModelSubstitutes(DeferredResult.class)
//                .genericModelSubstitutes(ResponseEntity.class)
//                .useDefaultResponseMessages(false)
//                .forCodeGeneration(false)
//                .pathMapping("/") // base，最终调用接口后会和paths拼接在一起
                // select()函数返回一个ApiSelectorBuilder实例,用来控制接口被swagger做成文档
                .select()
                //RequestHandlerSelectors 配置要扫描接口的方式
                // 扫描全部
                //.apis(RequestHandlerSelectors.any())
                // 都不扫描
                //.apis(RequestHandlerSelectors.none())
                // 扫描类上注解    参数是一个注解的反射对象
                //.apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
                // 扫描方法上的注解  比如RestController
                //.apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                // 指定要扫描的包, 用于指定扫描哪个包下的接口
                .apis(RequestHandlerSelectors.basePackage("com.sixbro.api.docs.swagger2.api"))
                // 选择所有的API,如果你想只为部分API生成文档，可以配置这里
                //.path() 过滤什么路径  只扫描
                .paths(PathSelectors.any())
                .build()
                .globalOperationParameters(parameters);
    }

    /**
     * 用于定义API主界面的信息，比如可以声明所有的API的总标题、描述、版本
     * @return
     */
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                // 可以用来自定义API的主标题【文档标题】
                .title(properties.getApiTitle())
                // 可以用来描述整体的API【文档描述】
                .description(properties.getApiDescription())
                // 用于定义服务的域名【服务条款URL】
                .termsOfServiceUrl(properties.getApiTermsOfServiceUrl())
                // 可以用来定义版本。
                .version(properties.getApiVersion())
                // 链接显示文字
                .license(properties.getApiLicense())
                // 网站链接
                .licenseUrl(properties.getApiLicenseUrl())
                // 作者信息
                .contact(new Contact(properties.getApiContact().getName(), properties.getApiContact().getUrl(), properties.getApiContact().getEmail()))
                .build();
    }
}