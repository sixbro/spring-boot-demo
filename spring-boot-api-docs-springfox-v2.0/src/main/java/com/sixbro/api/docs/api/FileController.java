package com.sixbro.api.docs.api;

import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 *  文件管理
 * </p>
 *
 * @Author: Mr.Lu
 * @Since: 2022/1/4 10:17
 */
@Api(tags = "文件管理")
@RequestMapping("/file")
@RestController
public class FileController {

    @ApiOperation(value = "上传文件",notes = "有文件上传时要用@ApiParam，用法基本与@ApiImplicitParam一样，不过@ApiParam用在参数上。 或者你也可以不注解，swagger会自动生成说明")
    @PostMapping(value = "/upload")
    public String upload(@ApiParam(value = "图片文件", required = true) MultipartFile uploadFile){
        String originalFilename = uploadFile.getOriginalFilename();

        return originalFilename;
    }

    //
    @ApiOperation(value = "上传多个文件",notes = "多个文件上传时，**swagger只能测试单文件上传**")
    @PostMapping(value = "/upload2",consumes = "multipart/*", headers = "content-type=multipart/form-data")
    public String upload2(@ApiParam(value = "图片文件", required = true,allowMultiple = true)MultipartFile[] uploadFile){
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < uploadFile.length; i++) {
            System.out.println(uploadFile[i].getOriginalFilename());
            sb.append(uploadFile[i].getOriginalFilename());
            sb.append(",");
        }
        return sb.toString();
    }

    @ApiOperation(value = "既有文件，又有参数",notes = "既有文件，又有参数")
    @PostMapping(value = "/upload3")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name",
                    value = "图片新名字",
                    required = true
            )
    })
    public String upload3(@ApiParam(value = "图片文件", required = true) MultipartFile uploadFile, String name){
        String originalFilename = uploadFile.getOriginalFilename();

        return originalFilename + ":" + name;
    }
}
