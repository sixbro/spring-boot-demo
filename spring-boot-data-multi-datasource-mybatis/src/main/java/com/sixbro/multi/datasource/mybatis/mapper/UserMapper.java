package com.sixbro.multi.datasource.mybatis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sixbro.multi.datasource.mybatis.model.User;

/**
 * <p>
 *  数据访问层
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2022/1/15 21:53
 */
public interface UserMapper extends BaseMapper<User> {
}
