package com.sixbro.multi.datasource.mybatis.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2022/1/15 21:38
 */
@Configuration
@MapperScan(basePackages = "com.sixbro.multi.datasource.mybatis.mapper")
public class MybatisConfig {
}
