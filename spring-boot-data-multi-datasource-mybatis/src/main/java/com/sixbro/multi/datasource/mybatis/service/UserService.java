package com.sixbro.multi.datasource.mybatis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sixbro.multi.datasource.mybatis.model.User;

/**
 * <p>
 *  数据服务层
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2022/1/15 21:53
 */
public interface UserService extends IService<User> {

    /**
     * 添加 User
     *
     * @param user 用户
     */
    void addUser(User user);
}
