package com.sixbro.multi.datasource.mybatis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2022/1/15 21:37
 */
@SpringBootApplication
public class MultiDatasourceMybatisApplication {

    public static void main(String[] args) {
        SpringApplication.run(MultiDatasourceMybatisApplication.class, args);
    }
}
