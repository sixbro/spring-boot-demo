## spring-boot-ratelimit-redis

> 此 demo 主要演示了 Spring Boot 项目如何通过 AOP 结合 Redis + Lua 脚本实现分布式限流，旨在保护 API 被恶意频繁访问的问题，是 `spring-boot-demo-ratelimit-guava` 的升级版。


## 参考

- [mica-plus-redis 的分布式限流实现](https://github.com/lets-mica/mica/tree/master/mica-plus-redis)
- [Java并发：分布式应用限流 Redis + Lua 实践](https://segmentfault.com/a/1190000016042927)