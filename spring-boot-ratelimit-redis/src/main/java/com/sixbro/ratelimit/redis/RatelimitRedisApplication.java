package com.sixbro.ratelimit.redis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-15 16:41
 */
@SpringBootApplication
public class RatelimitRedisApplication {

    public static void main(String[] args) {
        SpringApplication.run(RatelimitRedisApplication.class, args);
    }
}
