package com.sixbro.websocket.config;

import org.apache.commons.lang3.time.DateUtils;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

/**
 * @author: Mr.Lu
 * @since: 2022/1/16 11:51
 */
public class SockTaskRunner implements Runnable {

    @Override
    public void run() {
        Iterator<SocketSessionInfo> it = SocketSessionHandler.sessions.iterator();
        while(it.hasNext()) {
            SocketSessionInfo info = it.next();

            if(DateUtils.truncatedCompareTo(new Date(), info.getEndSessionTime(), Calendar.MINUTE) > 0) {
                try {
                    info.getWebSocketSession().close();
                    SocketSessionHandler.newInstance().removeSession(info.getSessionId());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
