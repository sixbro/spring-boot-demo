package com.sixbro.websocket.service;

/**
 * @author: Mr.Lu
 * @since: 2022/1/16 11:51
 */
public class EchoServiceImpl implements EchoService {
    private final String echoFormat;

    public EchoServiceImpl(String echoFormat) {
        this.echoFormat = (echoFormat != null) ? echoFormat : "%s";
    }

    @Override
    public String getMessage(String message) {
        return String.format(this.echoFormat, message);
    }
}
