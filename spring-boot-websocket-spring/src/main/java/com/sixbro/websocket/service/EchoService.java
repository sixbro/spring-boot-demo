package com.sixbro.websocket.service;

/**
 * @author: Mr.Lu
 * @since: 2022/1/16 11:51
 */
public interface EchoService {

    String getMessage(String message);
}
