package com.sixbro.websocket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2022/1/16 11:51
 */
@SpringBootApplication
public class WebsocketSpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebsocketSpringApplication.class, args);
    }
}
