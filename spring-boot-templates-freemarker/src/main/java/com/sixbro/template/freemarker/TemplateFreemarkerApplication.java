package com.sixbro.template.freemarker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 *
 * </p>
 *
 * @Author: Mr.Lu
 * @Since: 2022/1/4 16:44
 */
@SpringBootApplication
public class TemplateFreemarkerApplication {

    public static void main(String[] args) {
        SpringApplication.run(TemplateFreemarkerApplication.class, args);
    }
}
