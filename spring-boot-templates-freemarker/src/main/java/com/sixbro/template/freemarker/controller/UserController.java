package com.sixbro.template.freemarker.controller;

import com.sixbro.template.freemarker.model.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 用户页面
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2020/6/26 13:58
 */
@Slf4j
@Controller
@RequestMapping(path = {"/user"})
public class UserController {

    @PostMapping("/login")
    public ModelAndView login(User user, HttpServletRequest request) {
        ModelAndView view = new ModelAndView();

        view.addObject(user);
        view.setViewName("redirect:/");

        request.getSession().setAttribute("user", user);
        return view;
    }

    @GetMapping("/login")
    public ModelAndView login() {
        return new ModelAndView("page/login");
    }
}
