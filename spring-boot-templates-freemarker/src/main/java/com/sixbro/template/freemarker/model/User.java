package com.sixbro.template.freemarker.model;

import lombok.Data;

/**
 * <p>
 * 用户 model
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2020/6/26 13:56
 */
@Data
public class User {
    private String name;
    private String password;
}
