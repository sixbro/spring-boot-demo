package com.sixbro.template.freemarker.controller;

import cn.hutool.core.util.ObjectUtil;
import com.sixbro.template.freemarker.model.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 主页
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2020/6/26 13:57
 */
@Slf4j
@Controller
public class IndexController {

    @GetMapping(value = {"", "/"})
    public ModelAndView index(HttpServletRequest request) {
        ModelAndView view = new ModelAndView();

        User user = (User) request.getSession().getAttribute("user");
        if (ObjectUtil.isNull(user)) {
            view.setViewName("redirect:/user/login");
        } else {
            view.setViewName("page/index");
            view.addObject(user);
        }

        return view;
    }
}
