# spring-boot-open-api-docs-v3.0-springdoc

> 此 demo 主要演示了在Spring Boot单体架构下集成Knife4j，基于springdoc-openapi以及OpenAPIV3

## 参考

- [springdoc-openapi 文档](https://springdoc.org/)
- [springdoc-openapi Demo Applications](https://github.com/springdoc/springdoc-openapi-demos.git)