package com.sixbro.api.docs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Objects;
import java.util.StringJoiner;

/**
 * <p>
 *
 * </p>
 *
 * @Author: Mr.Lu
 * @Since: 2022/1/5 10:00
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-12-02T19:53:02.467132-01:00[Atlantic/Azores]")
@JacksonXmlRootElement(localName = "user")
@XmlRootElement(name = "user")
@XmlAccessorType(XmlAccessType.FIELD)
public class User {

    @JsonProperty("id")
    @JacksonXmlProperty(localName = "id")
    private Long id;

    @JsonProperty("username")
    @JacksonXmlProperty(localName = "username")
    private String username;

    @JsonProperty("password")
    @JacksonXmlProperty(localName = "password")
    private String password;

    @JsonProperty("firstName")
    @JacksonXmlProperty(localName = "firstName")
    private String firstName;

    @JsonProperty("lastName")
    @JacksonXmlProperty(localName = "lastName")
    private String lastName;

    @JsonProperty("email")
    @JacksonXmlProperty(localName = "email")
    private String email;

    @JsonProperty("phone")
    @JacksonXmlProperty(localName = "phone")
    private String phone;

    @JsonProperty("state")
    @JacksonXmlProperty(localName = "state")
    private Integer state;

    @Schema(description = "主键id", example = "1")
    public Long getId() {
        return id;
    }

    @Schema(description = "用户名", example = "admin")
    public String getUsername() {
        return username;
    }

    @Schema(description = "密码", defaultValue = "123456", example = "123456")
    public String getPassword() {
        return password;
    }

    @Schema(description = "first name 就是 given name 是名", example = "亮")
    public String getFirstName() {
        return firstName;
    }

    @Schema(description = "last name 就是 family name 是姓", example = "诸葛")
    public String getLastName() {
        return lastName;
    }

    @Schema(description = "Email", example = "sixbro@163.com")
    public String getEmail() {
        return email;
    }

    @Schema(description = "联系电话", example = "1388888888")
    public String getPhone() {
        return phone;
    }

    @Schema(description = "用户状态", defaultValue = "1")
    public Integer getState() {
        return state;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User id(Long id) {
        this.id = id;
        return this;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public User username(String username) {
        this.username = username;
        return this;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User password(String password) {
        this.password = password;
        return this;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public User firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public User lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public User email(String email) {
        this.email = email;
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public User phone(String phone) {
        this.phone = phone;
        return this;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public User state(Integer state) {
        this.state = state;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return Objects.equals(getId(), user.getId())
                && Objects.equals(getUsername(), user.getUsername())
                && Objects.equals(getPassword(), user.getPassword())
                && Objects.equals(getFirstName(), user.getFirstName())
                && Objects.equals(getLastName(), user.getLastName())
                && Objects.equals(getEmail(), user.getEmail())
                && Objects.equals(getPhone(), user.getPhone())
                && Objects.equals(getState(), user.getState());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getUsername(), getPassword(), getFirstName(), getLastName(), getEmail(), getPhone(), getState());
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", User.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("username='" + username + "'")
                .add("password='" + password + "'")
                .add("firstName='" + firstName + "'")
                .add("lastName='" + lastName + "'")
                .add("email='" + email + "'")
                .add("phone='" + phone + "'")
                .add("state=" + state)
                .toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
