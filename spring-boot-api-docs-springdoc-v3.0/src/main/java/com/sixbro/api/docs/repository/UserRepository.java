package com.sixbro.api.docs.repository;

import com.sixbro.api.docs.model.User;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *
 * </p>
 *
 * @Author: Mr.Lu
 * @Since: 2022/1/6 11:58
 */
@Repository
public class UserRepository extends HashMapRepository<User, String> {

    public UserRepository() {
        super(User.class);
    }

    @Override
    <S extends User> String getEntityId(S user) {
        return user.getUsername();
    }

    @Override
    public void deleteAllById(Iterable<? extends String> iterable) {

    }
}