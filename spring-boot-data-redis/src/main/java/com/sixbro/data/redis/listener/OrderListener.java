package com.sixbro.data.redis.listener;

import com.sixbro.data.redis.annotation.RedisMessageListener;
import org.springframework.stereotype.Service;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-18 16:03
 */
@Service
public class OrderListener {

    @RedisMessageListener(topic = "order::getState")
    public void getState(String message) {
        System.out.println("OrderListener --->  getState ---->" + message);
    }

    @RedisMessageListener(topic = "order::getState")
    public void onMessage(String message){
        System.out.println("OrderListener --->  onMessage ---->" + message);
    }
}
