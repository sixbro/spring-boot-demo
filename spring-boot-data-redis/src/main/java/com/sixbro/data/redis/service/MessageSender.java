package com.sixbro.data.redis.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-18 15:54
 */
@EnableScheduling // 开启定时器功能
@Component
public class MessageSender {

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 间隔5s 通过StringRedisTemplate对象向redis消息队列chat频道发布消息
     */
    @Scheduled(fixedRate = 5000)
    public void sendMessage(){
        redisTemplate.convertAndSend("chat", "hello "+ new Date());
    }

    public void sendMsg(String msg){
        redisTemplate.convertAndSend("onMessage", msg);
        redisTemplate.convertAndSend("onMsg", msg);
    }
}
