package com.sixbro.data.redis.api;

import com.sixbro.data.redis.enums.RedisTopic;
import com.sixbro.data.redis.service.RedisService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-20 10:25
 */
@RestController
public class MessageController {

    private final RedisService redisService;

    public MessageController(RedisService redisService) {
        this.redisService = redisService;
    }

    @PostMapping("/api/push")
    public void publish(String message){
        redisService.publish(RedisTopic.TOPIC_DISCOVER.getTopic(), message);
    }

    @PostMapping("/api/opt")
    public void set(){
        redisService.set("test", "测试值过期", 120, TimeUnit.SECONDS);
    }
}
