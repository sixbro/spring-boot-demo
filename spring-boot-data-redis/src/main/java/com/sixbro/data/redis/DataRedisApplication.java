package com.sixbro.data.redis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-18 14:05
 */
@SpringBootApplication
public class DataRedisApplication {

    public static void main(String[] args) {
        SpringApplication.run(DataRedisApplication.class, args);
    }
}
