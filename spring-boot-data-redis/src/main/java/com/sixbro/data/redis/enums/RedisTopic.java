package com.sixbro.data.redis.enums;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  定义不同主题类型
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021/6/30 14:37
 */
public enum RedisTopic {
    /**
     * redis主题名称定义 需要与发布者一致
     *
     */
    TOPIC_DISCOVERY("topic:discovery", "设备发现变更Topic"),

    TOPIC_HEALTHY("topic:healthy", "健康扫描的设备Topic"),

    TOPIC_SETTINGS("topic:settings",  "配置扫描变更的设备Topic"),

    TOPIC_DISCOVER("topic:discover", "发现设备Topic"),
    ;

    public final static List<String> topicList = Arrays.stream(values()).map(RedisTopic::getTopic).collect(Collectors.toList());

    /**
     * 主题名称
     */
    private final String topic;
    /**
     * 描述
     */
    private final String description;

    RedisTopic(String topic, String description) {
        this.topic = topic;
        this.description = description;
    }


    public String getTopic() {
        return topic;
    }

    public String getDescription() {
        return description;
    }
}
