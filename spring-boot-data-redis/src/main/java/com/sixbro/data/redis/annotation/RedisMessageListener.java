package com.sixbro.data.redis.annotation;

import java.lang.annotation.*;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-18 16:01
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RedisMessageListener {

    /**
     * 事件主题
     * @return
     */
    String topic();
}
