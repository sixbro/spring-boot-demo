package com.sixbro.data.redis.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-18 15:50
 */
@Service
public class MessageReceiver {
    private static final Logger log = LoggerFactory.getLogger(MessageReceiver.class);


    public void receiveMessage(String message) {
        System.out.println("接收消息：" + message);
    }

    public void onMsg(String jsonMsg) {
        // 具体回调的方法
        log.info("onMsg ----->  {}", jsonMsg);
    }

    public void onMessage(String jsonMsg) {
        // 具体回调的方法
        log.info("onMessage ----->  {}", jsonMsg);
    }
}
