package com.sixbro.template.beetl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 *
 * </p>
 *
 * @Author: Mr.Lu
 * @Since: 2022/1/4 16:34
 */
@SpringBootApplication
public class TemplateBeetlApplication {

    public static void main(String[] args) {
        SpringApplication.run(TemplateBeetlApplication.class, args);
    }
}
