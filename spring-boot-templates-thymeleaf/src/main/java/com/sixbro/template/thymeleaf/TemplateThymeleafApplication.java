package com.sixbro.template.thymeleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 *  启动类
 * </p>
 *
 * @Author: Mr.Lu
 * @Since: 2022/1/4 16:56
 */
@SpringBootApplication
public class TemplateThymeleafApplication {

    public static void main(String[] args) {
        SpringApplication.run(TemplateThymeleafApplication.class, args);
    }
}
