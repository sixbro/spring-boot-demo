package com.sixbro.template.thymeleaf.model;

import lombok.Data;

/**
 * <p>
 * 用户 model
 * </p>
 *
 * @Author: Mr.Lu
 * @Since: 2022/1/4 16:56
 */
@Data
public class User {
    private String name;
    private String password;
}
