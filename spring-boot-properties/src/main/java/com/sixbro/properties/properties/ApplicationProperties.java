package com.sixbro.properties.properties;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * <p>
 *  项目配置
 * </p>
 *
 * @Author: Mr.Lu
 * @Since: 2022/1/4 11:41
 */
@Data
@Component
public class ApplicationProperties {
    @Value("${application.name}")
    private String name;
    @Value("${application.version}")
    private String version;
}
