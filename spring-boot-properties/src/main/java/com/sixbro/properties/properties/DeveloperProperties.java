package com.sixbro.properties.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * <p>
 *  开发人员配置信息
 * </p>
 *
 * @Author: Mr.Lu
 * @Since: 2022/1/4 11:42
 */
@Data
@Component
@ConfigurationProperties(prefix = DeveloperProperties.PREFIX)
public class DeveloperProperties {
    public static final String PREFIX = "developer";

    private String name;
    private String website;
    private String qq;
    private String phone;
}