package com.sixbro.properties.api;

import cn.hutool.core.lang.Dict;
import com.sixbro.properties.properties.ApplicationProperties;
import com.sixbro.properties.properties.DeveloperProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * <p>
 *  测试Controller
 * </p>
 *
 * @Author: Mr.Lu
 * @Since: 2022/1/4 11:42
 */
public class PropertiesController {
    private final ApplicationProperties applicationProperties;
    private final DeveloperProperties developerProperties;

    @Autowired
    public PropertiesController(ApplicationProperties applicationProperties, DeveloperProperties developerProperties) {
        this.applicationProperties = applicationProperties;
        this.developerProperties = developerProperties;
    }

    @GetMapping("/property")
    public Dict index() {
        return Dict.create().set("applicationProperty", applicationProperties).set("developerProperty", developerProperties);
    }
}
