package com.sixbro.mq.kafka.constants;

/**
 * <p>
 *  kafka 常量池
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-25 11:46
 */
public interface KafkaConsts {
    /**
     * 默认分区大小
     */
    Integer DEFAULT_PARTITION_NUM = 3;

    /**
     * Topic 名称
     */
    String TOPIC_TEST = "test";
}
