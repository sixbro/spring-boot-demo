package com.sixbro.mq.kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-25 11:44
 */
@SpringBootApplication
public class MQKafkaApplication {

    public static void main(String[] args) {
        SpringApplication.run(MQKafkaApplication.class, args);
    }
}
