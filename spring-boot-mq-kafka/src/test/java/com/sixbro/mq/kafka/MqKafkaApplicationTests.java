package com.sixbro.mq.kafka;

import com.sixbro.mq.kafka.constants.KafkaConsts;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.KafkaTemplate;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-25 11:49
 */
@SpringBootTest
public class MqKafkaApplicationTests {

    @Autowired
    private KafkaTemplate<String, String> template;

    /**
     * 测试发送消息
     */
    @Test
    public void testSend() {
        template.send(KafkaConsts.TOPIC_TEST, "hello,kafka...");
    }
}
