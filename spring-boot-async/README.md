## spring-boot-demo-async

> 此 demo 主要演示了 Spring Boot 如何使用原生提供的异步任务支持，实现异步执行任务。

## 运行结果

### 异步任务

```bash
2021-12-17 11:25:47.545  INFO 1116 --- [   async-task-1] com.sixbro.async.task.TaskFactory        : asyncTask1开始执行，当前线程名称【async-task-1】
2021-12-17 11:25:47.546  INFO 1116 --- [   async-task-3] com.sixbro.async.task.TaskFactory        : asyncTask3开始执行，当前线程名称【async-task-3】
2021-12-17 11:25:47.546  INFO 1116 --- [   async-task-2] com.sixbro.async.task.TaskFactory        : asyncTask2开始执行，当前线程名称【async-task-2】
2021-12-17 11:25:49.555  INFO 1116 --- [   async-task-2] com.sixbro.async.task.TaskFactory        : asyncTask2执行成功，当前线程名称【async-task-2】
2021-12-17 11:25:50.573  INFO 1116 --- [   async-task-3] com.sixbro.async.task.TaskFactory        : asyncTask3执行成功，当前线程名称【async-task-3】
2021-12-17 11:25:52.552  INFO 1116 --- [   async-task-1] com.sixbro.async.task.TaskFactory        : asyncTask1执行成功，当前线程名称【async-task-1】
2021-12-17 11:25:52.552  INFO 1116 --- [           main] com.sixbro.async.AsyncApplicationTests   : 异步任务全部执行结束，总耗时：5140 毫秒
```

### 同步任务

```bash
2021-12-17 11:33:37.204  INFO 5060 --- [           main] com.sixbro.async.task.TaskFactory        : task1开始执行，当前线程名称【main】
2021-12-17 11:33:42.209  INFO 5060 --- [           main] com.sixbro.async.task.TaskFactory        : task1执行成功，当前线程名称【main】
2021-12-17 11:33:42.209  INFO 5060 --- [           main] com.sixbro.async.task.TaskFactory        : task2开始执行，当前线程名称【main】
2021-12-17 11:33:44.210  INFO 5060 --- [           main] com.sixbro.async.task.TaskFactory        : task2执行成功，当前线程名称【main】
2021-12-17 11:33:44.210  INFO 5060 --- [           main] com.sixbro.async.task.TaskFactory        : task3开始执行，当前线程名称【main】
2021-12-17 11:33:47.210  INFO 5060 --- [           main] com.sixbro.async.task.TaskFactory        : task3执行成功，当前线程名称【main】
2021-12-17 11:33:47.210  INFO 5060 --- [           main] com.sixbro.async.AsyncApplicationTests   : 同步任务全部执行结束，总耗时：10032 毫秒
```

## 参考

- Spring Boot 异步任务线程池的配置 参考官方文档：https://docs.spring.io/spring-boot/docs/2.1.0.RELEASE/reference/htmlsingle/#boot-features-task-execution-scheduling