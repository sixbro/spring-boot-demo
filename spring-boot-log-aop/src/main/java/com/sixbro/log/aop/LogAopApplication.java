package com.sixbro.log.aop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-24 17:27
 */
@SpringBootApplication
public class LogAopApplication {

    public static void main(String[] args) {
        SpringApplication.run(LogAopApplication.class, args);
    }
}
