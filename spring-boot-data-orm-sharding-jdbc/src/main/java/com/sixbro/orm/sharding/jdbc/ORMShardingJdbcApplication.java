package com.sixbro.orm.sharding.jdbc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2022/1/16 10:55
 */
@SpringBootApplication
public class ORMShardingJdbcApplication {

    public static void main(String[] args) {
        SpringApplication.run(ORMShardingJdbcApplication.class, args);
    }
}
