package com.sixbro.orm.sharding.jdbc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sixbro.orm.sharding.jdbc.model.Order;
import org.springframework.stereotype.Component;

/**
 * <p>
 * 订单表 Mapper
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2022/1/16 10:55
 */
@Component
public interface OrderMapper extends BaseMapper<Order> {
}
