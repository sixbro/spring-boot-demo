# spring-boot-samples-orm-sharding-jdbc

> 本 demo 主要演示了如何集成 `sharding-jdbc` 实现分库分表操作，ORM 层使用了`Mybatis-Plus`简化开发，童鞋们可以按照自己的喜好替换为 JPA、通用Mapper、JdbcTemplate甚至原生的JDBC都可以。
>
> PS：
>
> 1. 目前当当官方提供的starter存在bug，版本号：`3.1.0`，因此本demo采用手动配置。
> 2. 文档真的很垃圾​ :joy:

## 1. 运行方式

1. 在数据库创建2个数据库，分别为：`spring-boot-samples`、`spring-boot-samples-2`
2. 去数据库执行 `sql/schema.sql` ，创建 `6` 张分片表
3. 找到 `DataSourceShardingConfig` 配置类，修改 `数据源` 的相关配置，位于 `dataSourceMap()` 这个方法
4. 找到测试类 `SpringBootSamplesOrmShardingJdbcApplicationTests` 进行测试

## 3. 参考

1. `ShardingSphere` 官网：https://shardingsphere.apache.org/index_zh.html (虽然文档确实垃圾，但是还是得参考啊~)
2. `Mybatis-Plus` 语法参考官网：https://mybatis.plus/