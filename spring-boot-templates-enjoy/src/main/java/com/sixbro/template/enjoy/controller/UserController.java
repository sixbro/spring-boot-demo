package com.sixbro.template.enjoy.controller;

import com.sixbro.template.enjoy.model.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 用户页面
 * </p>
 *
 * @Author: Mr.Lu
 * @Since: 2022/1/4 16:39
 */
@Slf4j
@Controller
@RequestMapping(path = {"/user"})
public class UserController {

    @PostMapping("/login")
    public ModelAndView login(User user, HttpServletRequest request) {
        ModelAndView view = new ModelAndView();

        view.addObject(user);
        view.setViewName("redirect:/");

        request.getSession().setAttribute("user", user);
        return view;
    }

    @GetMapping("/login")
    public ModelAndView login() {
        return new ModelAndView("page/login");
    }
}
