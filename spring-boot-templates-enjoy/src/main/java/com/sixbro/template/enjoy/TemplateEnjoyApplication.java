package com.sixbro.template.enjoy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 *
 * </p>
 *
 * @Author: Mr.Lu
 * @Since: 2022/1/4 16:39
 */
@SpringBootApplication
public class TemplateEnjoyApplication {

    public static void main(String[] args) {
        SpringApplication.run(TemplateEnjoyApplication.class, args);
    }
}
