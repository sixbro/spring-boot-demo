# spring-boot-ldap

> 此 demo 主要演示了 Spring Boot 如何集成 `spring-boot-starter-data-ldap` 完成对 LDAP 的基本 CURD操作, 并给出以登录为实战的 API 示例

## docker openldap 安装步骤

> 参考： https://github.com/osixia/docker-openldap
1. 下载镜像： `docker pull osixia/openldap:1.2.5`

2. 运行容器： `docker run -p 389:389 -p 636:636 --name my-openldap --detach osixia/openldap:1.2.5`

3. 添加管理员： `docker exec my-openldap ldapsearch -x -H ldap://localhost -b dc=example,dc=org -D "cn=admin,dc=example,dc=org" -w admin`

4. 停止容器：`docker stop my-openldap`

5. 启动容器：`docker start my-openldap`


## 其余代码参见本 demo

## 参考

spring-data-ldap 官方文档: https://docs.spring.io/spring-data/ldap/docs/2.6.0/reference/html/#reference