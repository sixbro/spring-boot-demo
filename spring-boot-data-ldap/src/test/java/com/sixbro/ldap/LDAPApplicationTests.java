package com.sixbro.ldap;

import com.sixbro.ldap.api.ApiResult;
import com.sixbro.ldap.entity.Person;
import com.sixbro.ldap.request.LoginRequest;
import com.sixbro.ldap.service.PersonService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-20 17:53
 */
@SpringBootTest
public class LDAPApplicationTests {


    @Resource
    private PersonService personService;

    @Test
    public void contextLoads() {
    }

    /**
     * 测试查询单个
     */
    @Test
    public void loginTest() {
        LoginRequest loginRequest = LoginRequest.builder().username("wangwu").password("123456").build();
        ApiResult login = personService.login(loginRequest);
        System.out.println(login);
    }

    /**
     * 测试查询列表
     */
    @Test
    public void listAllPersonTest() {
        ApiResult result = personService.listAllPerson();
        System.out.println(result);
    }

    /**
     * 测试保存
     */
    @Test
    public void saveTest() {
        Person person = new Person();

        person.setUid("zhaosi");

        person.setSurname("赵");
        person.setGivenName("四");
        person.setUserPassword("123456");

        // required field
        person.setPersonName("赵四");
        person.setUidNumber("666");
        person.setGidNumber("666");
        person.setHomeDirectory("/home/zhaosi");
        person.setLoginShell("/bin/bash");

        personService.save(person);
    }

    /**
     * 测试删除
     */
    @Test
    public void deleteTest() {
        Person person = new Person();
        person.setUid("zhaosi");

        personService.delete(person);
    }
}
