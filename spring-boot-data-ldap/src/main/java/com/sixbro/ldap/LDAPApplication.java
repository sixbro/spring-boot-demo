package com.sixbro.ldap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-20 17:52
 */
@SpringBootApplication
public class LDAPApplication {

    public static void main(String[] args) {
        SpringApplication.run(LDAPApplication.class, args);
    }
}
