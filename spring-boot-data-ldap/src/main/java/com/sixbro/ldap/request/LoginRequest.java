package com.sixbro.ldap.request;

import lombok.Builder;
import lombok.Data;

/**
 * <p>
 *  LoginRequest
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-24 17:03
 */
@Data
@Builder
public class LoginRequest {

    private String username;

    private String password;
}
