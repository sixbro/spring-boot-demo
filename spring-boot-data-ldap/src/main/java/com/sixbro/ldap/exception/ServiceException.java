package com.sixbro.ldap.exception;


import com.sixbro.ldap.api.ApiResultCode;
import lombok.Getter;

/**
 * <p>
 *  ServiceException
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-24 16:50
 */
public class ServiceException extends RuntimeException {
    @Getter
    private int code;

    @Getter
    private String message;

    public ServiceException(ApiResultCode apiResultCode) {
        this(apiResultCode.getCode(), apiResultCode.getMessage());
    }

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(Integer code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }

    public ServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
