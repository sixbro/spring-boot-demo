package com.sixbro.ldap.service;

import com.sixbro.ldap.api.ApiResult;
import com.sixbro.ldap.entity.Person;
import com.sixbro.ldap.request.LoginRequest;

/**
 * <p>
 *  PersonService
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-24 17:04
 */
public interface PersonService {
    /**
     * 登录
     *
     * @param request {@link LoginRequest}
     * @return {@link ApiResult}
     */
    ApiResult login(LoginRequest request);

    /**
     * 查询全部
     *
     * @return {@link ApiResult}
     */
    ApiResult listAllPerson();

    /**
     * 保存
     *
     * @param person {@link Person}
     */
    void save(Person person);

    /**
     * 删除
     *
     * @param person {@link Person}
     */
    void delete(Person person);

}
