package com.sixbro.ldap.api;

/**
 * <p>
 *  ApiResultCode
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-24 16:48
 */
public enum ApiResultCode {
    /**
     * 接口调用成功
     */
    SUCCESS(0, "Request Successful"),

    /**
     * 服务器暂不可用，建议稍候重试。建议重试次数不超过3次。
     */
    FAILURE(-1, "System Busy"),

    ;

    public final int code;
    public final String message;

    ApiResultCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
