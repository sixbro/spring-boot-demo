## spring-boot-cache-redis

> 此 demo 主要演示了 Spring Boot 如何整合 redis，操作redis中的数据，并使用redis缓存数据。连接池使用  Lettuce。


## 参考

- spring-data-redis 官方文档：https://docs.spring.io/spring-data/redis/docs/2.6.0/reference/html/#why-spring-redis
- redis 文档：https://redis.io/documentation
- redis 中文文档：http://www.redis.cn/commands.html