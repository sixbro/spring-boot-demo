package com.sixbro.exception.handler.exception;

import com.sixbro.exception.handler.constant.Status;
import lombok.Getter;

/**
 * <p>
 *  JSON异常
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-20 15:57
 */
@Getter
public class JSONException extends BaseException {


    public JSONException(Status status) {
        super(status);
    }

    public JSONException(Integer code, String message) {
        super(code, message);
    }
}
