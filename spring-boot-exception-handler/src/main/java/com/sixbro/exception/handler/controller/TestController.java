package com.sixbro.exception.handler.controller;

import com.sixbro.exception.handler.constant.Status;
import com.sixbro.exception.handler.exception.JSONException;
import com.sixbro.exception.handler.exception.PageException;
import com.sixbro.exception.handler.model.ApiResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * <p>
 * 测试Controller
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-20 16:01
 */
@Controller
public class TestController {

    @GetMapping("/json")
    @ResponseBody
    public ApiResponse jsonException() {
        throw new JSONException(Status.UNKNOWN_ERROR);
    }

    @GetMapping("/page")
    public ModelAndView pageException() {
        throw new PageException(Status.UNKNOWN_ERROR);
    }
}