# spring-boot-multi-datasource-jpa

>   此 demo 主要演示 Spring Boot 如何集成 JPA 的多数据源。

## 目录结构
```text
.
├── README.md
├── pom.xml
├── spring-boot-data-multi-datasource-jpa.iml
├── src
│   ├── main
│   │   ├── java
│   │   │   └── com.sixbro.data.multi.datasource.jpa
│   │   │       ├── SpringBootSamplesDataMultiDatasourceJpaApplication.java
│   │   │       ├── config
│   │   │       │   ├── PrimaryDataSourceConfig.java
│   │   │       │   ├── PrimaryJpaConfig.java
│   │   │       │   ├── SecondDataSourceConfig.java
│   │   │       │   ├── SecondJpaConfig.java
│   │   │       │   └── SnowflakeConfig.java
│   │   │       ├── entity
│   │   │       │   ├── primary
│   │   │       │   │   └── PrimaryMultiTable.java
│   │   │       │   └── second
│   │   │       │       └── SecondMultiTable.java
│   │   │       └── repository
│   │   │               ├── primary
│   │   │               │   └── PrimaryMultiTableRepository.java
│   │   │               └── second
│   │   │                   └── SecondMultiTableRepository.java
│   │   └── resources
│   │       └── application.yml
│   └── test
│       └── java
│           └── com.bxs.data.multi.datasource.jpa
│               └── SpringBootSamplesDataMultiDatasourceJpaApplicationTests.java
└── target
```

## 参考
1. https://www.jianshu.com/p/34730e595a8c
2. https://blog.csdn.net/anxpp/article/details/52274120