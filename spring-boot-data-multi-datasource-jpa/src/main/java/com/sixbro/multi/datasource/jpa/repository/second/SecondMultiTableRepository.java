package com.sixbro.multi.datasource.jpa.repository.second;

import com.sixbro.multi.datasource.jpa.entity.second.SecondMultiTable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * <p>
 * 多数据源测试 repo
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2022/1/15 17:27
 */
@Repository
public interface SecondMultiTableRepository extends JpaRepository<SecondMultiTable, Long> {
}
