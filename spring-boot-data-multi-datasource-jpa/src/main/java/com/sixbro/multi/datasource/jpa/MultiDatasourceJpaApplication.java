package com.sixbro.multi.datasource.jpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-25 15:16
 */
@SpringBootApplication
public class MultiDatasourceJpaApplication {

    public static void main(String[] args) {
        SpringApplication.run(MultiDatasourceJpaApplication.class, args);
    }
}
