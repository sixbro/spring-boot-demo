# spring-boot-open-api-docs-v3.0-springdoc-yaml

> 此 demo 主要演示了在Spring Boot单体架构下集成`openapi-springdoc-ui`，使用自定义 json/yml 文件而不是生成的文件

[OpenAPI Specification Guide](https://swagger.io/docs/specification/about/)