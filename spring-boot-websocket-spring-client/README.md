# spring-boot-websocket-spring-client

> 此 demo 主要演示了 Spring Boot 如何集成 WebSocket，实现后端主动往前端推送数据。网上大部分websocket的例子都是聊天室，本例主要是推送服务器状态信息。前端页面基于vue和element-ui实现。

## 2. 运行方式

1. 启动 `SpringBootSamplesWebsocketApplication.java`
2. 访问 http://localhost:8080/demo/server.html

## 3. 运行效果

![image-20181217110240322](assets/image-20181217110240322-5015760.png)

![image-20181217110304065](assets/image-20181217110304065-5015784.png)

![image-20181217110328810](assets/image-20181217110328810-5015808.png)

![image-20181217110336017](assets/image-20181217110336017-5015816.png)

## 4. 参考

### 4.1. 后端

1. Spring Boot 整合 Websocket 官方文档：https://docs.spring.io/spring/docs/5.2.6.RELEASE/spring-framework-reference/web.html#websocket
2. 服务器信息采集 oshi 使用：https://github.com/oshi/oshi

### 4.2. 前端

1. vue.js 语法：https://cn.vuejs.org/v2/guide/
2. element-ui 用法：http://element-cn.eleme.io/#/zh-CN
3. stomp.js 用法：https://github.com/jmesnil/stomp-websocket
4. sockjs 用法：https://github.com/sockjs/sockjs-client
5. axios.js 用法：https://github.com/axios/axios#example