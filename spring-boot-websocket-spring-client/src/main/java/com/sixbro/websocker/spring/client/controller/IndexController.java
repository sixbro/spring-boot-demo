package com.sixbro.websocker.spring.client.controller;

import com.sixbro.websocker.spring.client.model.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: Mr.Lu
 * @since: 2022/1/16 12:07
 */
@Controller
public class IndexController {
    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    /**
     * Index
     * @return
     */
    @RequestMapping(value={"", "/", "index"})
    public String index() {
        return "index";
    }

    @RequestMapping("chat")
    public String chat() {
        return "chat";
    }

    @RequestMapping("notifications")
    public String notifications() {
        return "notifications";
    }

    @RequestMapping("notifications-button")
    public String notificationsButton() {
        return "notifications-buttion";
    }

    @ResponseBody
    @RequestMapping(value = "/sendMessage", method = RequestMethod.POST)
    public void sendMessage() {
        List<Message> messages = getMessage();
        messagingTemplate.convertAndSend("/user/topic/message", messages);
    }

    private List<Message> getMessage(){
        List<Message> list = new ArrayList<>();
        list.add(new Message("Message-Text1"));
        list.add(new Message("Message-Text2"));
        list.add(new Message("Message-Text3"));
        list.add(new Message("Message-Text4"));
        return list;
    }
}
