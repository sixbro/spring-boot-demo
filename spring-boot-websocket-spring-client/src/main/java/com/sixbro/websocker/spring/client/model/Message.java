package com.sixbro.websocker.spring.client.model;

import java.io.Serializable;
import java.util.UUID;

/**
 * @author: Mr.Lu
 * @since: 2022/1/16 12:07
 */
public class Message implements Serializable {
    private static final long serialVersionUID = 6367726218945501780L;

    private String id;

    private String name;

    public Message(String name) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public Message setId(String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Message setName(String name) {
        this.name = name;
        return this;
    }
}
