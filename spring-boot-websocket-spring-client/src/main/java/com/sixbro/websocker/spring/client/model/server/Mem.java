package com.sixbro.websocker.spring.client.model.server;

import cn.hutool.core.util.NumberUtil;

/**
 * <p>
 * 內存相关信息实体
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2022/1/16 12:07
 */
public class Mem {
    /**
     * 内存总量
     */
    private double total;

    /**
     * 已用内存
     */
    private double used;

    /**
     * 剩余内存
     */
    private double free;

    public double getTotal() {
        return NumberUtil.div(total, (1024 * 1024 * 1024), 2);
    }

    public Mem setTotal(double total) {
        this.total = total;
        return this;
    }

    public double getUsed() {
        return NumberUtil.div(used, (1024 * 1024 * 1024), 2);
    }

    public Mem setUsed(double used) {
        this.used = used;
        return this;
    }

    public double getFree() {
        return NumberUtil.div(free, (1024 * 1024 * 1024), 2);
    }

    public Mem setFree(double free) {
        this.free = free;
        return this;
    }

    public double getUsage() {
        return NumberUtil.mul(NumberUtil.div(used, total, 4), 100);
    }
}
