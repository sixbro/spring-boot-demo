package com.sixbro.websocker.spring.client.common;

/**
 * <p>
 * WebSocket常量
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2022/1/16 12:07
 */
public interface WebSocketConsts {
    String PUSH_SERVER = "/topic/server";
}
