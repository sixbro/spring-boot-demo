package com.sixbro.websocker.spring.client.controller;

import cn.hutool.core.lang.Dict;
import com.sixbro.websocker.spring.client.model.Server;
import com.sixbro.websocker.spring.client.payload.ServerVO;
import com.sixbro.websocker.spring.client.utils.ServerUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 服务器监控Controller
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2022/1/16 12:07
 */
@RestController
@RequestMapping(path = {"/server"})
public class ServerController {

    @GetMapping
    public Dict serverInfo() throws Exception {
        Server server = new Server();
        server.copyTo();
        ServerVO serverVO = ServerUtils.wrapServerVO(server);
        return ServerUtils.wrapServerDict(serverVO);
    }
}
