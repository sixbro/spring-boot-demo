# spring-boot-websocket-socketio

> 此 demo 主要演示了 Spring Boot 如何使用 `netty-socketio` 集成 WebSocket，实现一个简单的聊天室。

## 2. 运行方式

1. 启动 `SpringBootSamplesWebsocketSocketioApplication.java`
2. 使用不同的浏览器，访问 http://localhost:8080/demo/index.html

## 3. 运行效果

**浏览器1：**![image-20181219152318079](assets/image-20181219152318079-5204198.png)

**浏览器2：**![image-20181219152330156](assets/image-20181219152330156-5204210.png)

## 4. 参考

### 4.1. 后端

1. Netty-socketio 官方仓库：https://github.com/mrniko/netty-socketio
2. SpringBoot系列 - 集成SocketIO实时通信：https://www.xncoding.com/2017/07/16/spring/sb-socketio.html
3. Spring Boot 集成 socket.io 后端实现消息实时通信：http://alexpdh.com/2017/09/03/springboot-socketio/
4. Spring Boot实战之netty-socketio实现简单聊天室：http://blog.csdn.net/sun_t89/article/details/52060946

### 4.2. 前端

1. socket.io 官网：https://socket.io/
2. axios.js 用法：https://github.com/axios/axios#example