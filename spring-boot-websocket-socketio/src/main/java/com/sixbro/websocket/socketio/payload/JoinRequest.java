package com.sixbro.websocket.socketio.payload;

import lombok.Data;

/**
 * <p>
 * 加群载荷
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2022/1/16 11:56
 */
@Data
public class JoinRequest {
    /**
     * 用户id
     */
    private String userId;

    /**
     * 群名称
     */
    private String groupId;
}
