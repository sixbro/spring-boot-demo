package com.sixbro.websocket.socketio.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * <p>
 * WebSocket配置类
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2022/1/16 11:56
 */
@Data
@ConfigurationProperties(prefix = WebSocketProperties.PREFIX)
public class WebSocketProperties {
    public static final String PREFIX = "ws.server";

    /**
     * 端口号
     */
    private Integer port;

    /**
     * host
     */
    private String host;
}
