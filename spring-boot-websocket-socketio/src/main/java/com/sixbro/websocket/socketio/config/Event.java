package com.sixbro.websocket.socketio.config;

/**
 * <p>
 * 事件常量
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2022/1/16 11:56
 */
public interface Event {
    /**
     * 聊天事件
     */
    String CHAT = "chat" ;

    /**
     * 广播消息
     */
    String BROADCAST = "broadcast" ;

    /**
     * 群聊
     */
    String GROUP = "group" ;

    /**
     * 加入群聊
     */
    String JOIN = "join" ;
}
