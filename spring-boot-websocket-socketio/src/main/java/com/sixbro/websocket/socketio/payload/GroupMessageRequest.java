package com.sixbro.websocket.socketio.payload;

import lombok.Data;

/**
 * <p>
 * 群聊消息载荷
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2022/1/16 11:56
 */
@Data
public class GroupMessageRequest {
    /**
     * 消息发送方用户id
     */
    private String fromUid;

    /**
     * 群组id
     */
    private String groupId;

    /**
     * 消息内容
     */
    private String message;
}
