package com.sixbro.websocket.socketio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2022/1/16 11:56
 */
@SpringBootApplication
public class WebsocketSocketioApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebsocketSocketioApplication.class, args);
    }
}
