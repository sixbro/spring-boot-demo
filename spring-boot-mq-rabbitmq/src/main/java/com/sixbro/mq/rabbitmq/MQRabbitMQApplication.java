package com.sixbro.mq.rabbitmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-25 11:52
 */
@SpringBootApplication
public class MQRabbitMQApplication {
    public static void main(String[] args) {
        SpringApplication.run(MQRabbitMQApplication.class, args);
    }
}
