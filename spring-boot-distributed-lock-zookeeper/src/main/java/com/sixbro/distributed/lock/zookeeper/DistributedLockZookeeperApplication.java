package com.sixbro.distributed.lock.zookeeper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 *
 * </p>
 *
 * @Author: Mr.Lu
 * @Since: 2022/1/4 15:49
 */
@SpringBootApplication
public class DistributedLockZookeeperApplication {

    public static void main(String[] args) {
        SpringApplication.run(DistributedLockZookeeperApplication.class, args);
    }
}
