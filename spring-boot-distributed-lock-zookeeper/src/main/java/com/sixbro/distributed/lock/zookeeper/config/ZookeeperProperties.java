package com.sixbro.distributed.lock.zookeeper.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * <p>
 * Zookeeper 配置项
 * </p>
 *
 * @Author: Mr.Lu
 * @Since: 2022/1/4 15:52
 */
@Data
@ConfigurationProperties(prefix = ZookeeperProperties.PREFIX)
public class ZookeeperProperties {
    public static final String PREFIX = "zk";

    /**
     * 连接地址
     */
    private String url;

    /**
     * 超时时间(毫秒)，默认1000
     */
    private int timeout = 1000;

    /**
     * 重试次数，默认3
     */
    private int retry = 3;
}
