package com.sixbro.distributed.lock.zookeeper.config;

import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <p>
 * Zookeeper配置类
 * </p>
 *
 * @Author: Mr.Lu
 * @Since: 2022/1/4 15:52
 */
@Configuration
@EnableConfigurationProperties(ZookeeperProperties.class)
public class ZookeeperConfig {
    private final ZookeeperProperties properties;

    @Autowired
    public ZookeeperConfig(ZookeeperProperties properties) {
        this.properties = properties;
    }

    @Bean
    public CuratorFramework curatorFramework() {
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(properties.getTimeout(), properties.getRetry());
        CuratorFramework client = CuratorFrameworkFactory.newClient(properties.getUrl(), retryPolicy);
        client.start();
        return client;
    }
}
