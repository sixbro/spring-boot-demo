package com.sixbro.task.quartz.common;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 通用Api封装
 * </p>
 *
 * @Author: Mr.Lu
 * @Since: 2022/1/4 17:21
 */
@Data
public class ApiResponse<T> implements Serializable {

    private int code;
    /**
     * 返回信息
     */
    private String message;
    /**
     * 返回数据
     */
    private T data;

    public ApiResponse() {
    }

    private ApiResponse(int code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    /**
     * 通用封装获取ApiResponse对象
     *
     * @param message 返回信息
     * @param data    返回数据
     * @return ApiResponse
     */
    public static <T>ApiResponse of(int code, String message, T data) {
        return new ApiResponse(code, message, data);
    }

    /**
     * 通用成功封装获取ApiResponse对象
     *
     * @param data 返回数据
     * @return ApiResponse
     */
    public static <T>ApiResponse ok(T data) {
        return new ApiResponse(200, "成功", data);
    }

    /**
     * 通用封装获取ApiResponse对象
     *
     * @param message 返回信息
     * @return ApiResponse
     */
    public static ApiResponse msg(String message) {
        return of(200, message, null);
    }
}
