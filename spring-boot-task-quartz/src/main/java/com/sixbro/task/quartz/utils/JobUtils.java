package com.sixbro.task.quartz.utils;

import com.sixbro.task.quartz.job.AbstractJob;

/**
 * <p>
 * 定时任务反射工具类
 * </p>
 *
 * @Author: Mr.Lu
 * @Since: 2020/6/23 12:52
 */
public class JobUtils {
    /**
     * 根据全类名获取Job实例
     *
     * @param classname Job全类名
     * @return {@link AbstractJob} 实例
     * @throws Exception 泛型获取异常
     */
    public static AbstractJob getClass(String classname) throws Exception {
        Class<?> clazz = Class.forName(classname);
        return (AbstractJob) clazz.newInstance();
    }
}
