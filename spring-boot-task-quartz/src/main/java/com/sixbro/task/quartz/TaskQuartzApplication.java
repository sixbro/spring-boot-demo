package com.sixbro.task.quartz;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 *
 * </p>
 *
 * @Author: Mr.Lu
 * @Since: 2022/1/4 17:21
 */
@MapperScan(basePackages = {"com.sixbro.task.quartz.mapper"})
@SpringBootApplication
public class TaskQuartzApplication {

    public static void main(String[] args) {
        SpringApplication.run(TaskQuartzApplication.class, args);
    }
}
