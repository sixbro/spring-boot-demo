package com.sixbro.api.docs.annotation;

import java.lang.annotation.*;

/**
 * <p>
 * 注解用于生成requestmappinginfo时候直接RR拼接路径
 * 规则，自动放置于路径开始部分；不做method做版本，避免难以维护
 * </p>
 *
 * @Author: Mr.Lu
 * @Since: 2022/1/7 18:00
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface ApiVersion {
    String value() default "";
}
