package com.sixbro.api.docs.advice;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *
 * </p>
 *
 * @Author: Mr.Lu
 * @Since: 2022/1/7 18:02
 */
@ControllerAdvice(annotations = RestController.class)
public class GlobalExceptionAdvice {
}
