package com.sixbro.api.docs.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.RequestParameterBuilder;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.schema.Example;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.Collections;

/**
 * <p>
 *
 * </p>
 *
 * @Author: Mr.Lu
 * @Since: 2022/1/7 17:14
 */
@EnableOpenApi
@Configuration
@Import(BeanValidatorPluginsConfiguration.class)
public class Knife4jConfig {

    @Bean
    public Docket defaultApi(){
        Docket docket = new Docket(DocumentationType.OAS_30)
                .apiInfo(apiInfo())
                .groupName("2")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.sixbro.api.docs.api.user"))
                .paths(PathSelectors.any())
                .build()
                .globalRequestParameters(Collections.singletonList(
                    new RequestParameterBuilder()
                            .name("X-API-VERSION")
                            .description("API 版本号")
                            .in(ParameterType.HEADER)
                            .required(true)
                            .example(new Example("X-API-VERSION", "X-API-VERSION", "API 版本", 1, "API 版本号", MediaType.APPLICATION_FORM_URLENCODED_VALUE))
                            .build()
                ))
                .securityContexts(Collections.singletonList(
                        SecurityContext
                                .builder()
                                .securityReferences(Collections.singletonList(
                                        SecurityReference
                                                .builder()
                                                .reference(HttpHeaders.AUTHORIZATION)
                                                .scopes(new AuthorizationScope[]{new AuthorizationScope("global", "accessEverything")})
                                                .build()
                                ))
                                // 声明作用域
                                .operationSelector(operationContext -> operationContext.requestMappingPattern().matches("/.*"))
                                .build()
                ))
                .securitySchemes(Collections.singletonList(
                        HttpAuthenticationScheme
                                .JWT_BEARER_BUILDER
                                .name(HttpHeaders.AUTHORIZATION)
                                .description("Bearer Token")
                                .build()
                ));
        return docket;
    }

    private ApiInfo apiInfo(){
        return new ApiInfoBuilder()
                .title("swagger-bootstrap-ui很棒~~~！！！")
                .description("swagger-bootstrap-ui-demo RESTful APIs")
                .termsOfServiceUrl("http://www.group.com/")
                .contact(new Contact("八一菜刀","http://gitee.com/xiaoymin","xiaoymin@foxmail.com"))
                .version("1.0")
                .build();
    }
}
