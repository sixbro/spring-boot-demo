package com.sixbro.api.docs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * <p>
 *
 * </p>
 *
 * @Author: Mr.Lu
 * @Since: 2022/1/7 17:05
 */
@SpringBootApplication
public class ApiDocsSpringfoxV3Knife4jApplication {
    public static final Logger logger = LoggerFactory.getLogger(ApiDocsSpringfoxV3Knife4jApplication.class);

    public static void main(String[] args) throws UnknownHostException {
        ConfigurableApplicationContext applicationContext = SpringApplication.run(ApiDocsSpringfoxV3Knife4jApplication.class, args);
        ConfigurableEnvironment environment = applicationContext.getEnvironment();
        String host= InetAddress.getLocalHost().getHostAddress();
        String port= environment.getProperty("server.port");
        logger.info("\n----------------------------------------------------------\n\t" +
                        "Application '{}' is running! Access URLs:\n\t" +
                        "Local: \t\thttp://localhost:{}\n\t" +
                        "External: \thttp://{}:{}\n\t"+
                        "Doc: \t\thttp://{}:{}/doc.html\n\t"+
                        "SwaggerDoc: \t\thttp://{}:{}/swagger-ui.html\n\t"+
                        "----------------------------------------------------------",
                environment.getProperty("spring.application.name"),
                environment.getProperty("server.port"),
                host, port,
                host, port,
                host, port);
    }
}
