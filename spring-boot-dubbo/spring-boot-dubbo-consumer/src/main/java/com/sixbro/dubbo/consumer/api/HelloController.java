package com.sixbro.dubbo.consumer.api;

import com.alibaba.dubbo.config.annotation.Reference;
import com.sixbro.dubbo.common.service.HelloService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  Hello服务API
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-25 11:36
 */
@Slf4j
@RestController
public class HelloController {
    @Reference
    private HelloService helloService;

    @GetMapping("/sayHello")
    public String sayHello(@RequestParam(defaultValue = "xkcoding") String name) {
        log.info("i'm ready to call someone......");
        return helloService.sayHello(name);
    }
}