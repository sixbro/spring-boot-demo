package com.sixbro.dubbo.common.service;

/**
 * <p>
 *  Hello服务接口
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-25 11:24
 */
public interface HelloService {

    /**
     * 问好
     *
     * @param name 姓名
     * @return 问好
     */
    String sayHello(String name);
}
