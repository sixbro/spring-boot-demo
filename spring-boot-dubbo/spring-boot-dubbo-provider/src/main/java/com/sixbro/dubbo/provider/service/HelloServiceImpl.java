package com.sixbro.dubbo.provider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.sixbro.dubbo.common.service.HelloService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * <p>
 *  Hello服务实现
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-25 11:33
 */
@Service
@Component
@Slf4j
public class HelloServiceImpl implements HelloService {
    /**
     * 问好
     *
     * @param name 姓名
     * @return 问好
     */
    @Override
    public String sayHello(String name) {
        log.info("someone is calling me......");
        return "say hello to: " + name;
    }
}