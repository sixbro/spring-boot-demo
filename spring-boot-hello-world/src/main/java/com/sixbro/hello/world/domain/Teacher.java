package com.sixbro.hello.world.domain;

import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @Author: Mr.Lu
 * @Since: 2022/1/12 15:42
 */
@Data
@Builder
public class Teacher {
    private Long id;
    private String name;
    private State state;
    private List<Student> students;
}
