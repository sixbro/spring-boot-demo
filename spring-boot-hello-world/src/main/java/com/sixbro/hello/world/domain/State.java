package com.sixbro.hello.world.domain;

/**
 * <p>
 *
 * </p>
 *
 * @Author: Mr.Lu
 * @Since: 2022/1/12 15:42
 */
public enum State {
    NORMAL, VACATION, RETIRED;
}
