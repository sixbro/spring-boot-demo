package com.sixbro.hello.world.domain;

import lombok.Builder;
import lombok.Data;

/**
 * <p>
 *
 * </p>
 *
 * @Author: Mr.Lu
 * @Since: 2022/1/12 15:42
 */
@Data
@Builder
public class Student {
    private Long id;
    private String name;
}