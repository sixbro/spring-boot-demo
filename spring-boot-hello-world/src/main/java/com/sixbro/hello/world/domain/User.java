package com.sixbro.hello.world.domain;

import lombok.Builder;
import lombok.Data;

/**
 * <p>
 *
 * </p>
 *
 * @Author: Mr.Lu
 * @Since: 2022/1/12 15:47
 */
@Data
@Builder
public class User {
    private Long id;
    private String name;
}
