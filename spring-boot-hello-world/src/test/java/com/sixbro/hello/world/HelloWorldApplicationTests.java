package com.sixbro.hello.world;

import com.alibaba.fastjson.JSON;
import com.sixbro.hello.world.domain.State;
import com.sixbro.hello.world.domain.Student;
import com.sixbro.hello.world.domain.Teacher;
import com.sixbro.hello.world.domain.User;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * <p>
 *
 * </p>
 *
 * @Author: Mr.Lu
 * @Since: 2022/1/12 15:41
 */
@SpringBootTest
public class HelloWorldApplicationTests {

    @Test
    public void test1() {
        List<Teacher> teachers = new ArrayList<>();
        teachers.add(Teacher.builder().id(System.currentTimeMillis()).name("张三").state(State.NORMAL).students(students()).build());
        teachers.add(Teacher.builder().id(System.currentTimeMillis()).name("李四").state(State.VACATION).students(students()).build());
        teachers.add(Teacher.builder().id(System.currentTimeMillis()).name("猴七").state(State.VACATION).students(new ArrayList<>()).build());
        teachers.add(Teacher.builder().id(System.currentTimeMillis()).name("王五").state(State.RETIRED).students(students()).build());
        teachers.add(Teacher.builder().id(System.currentTimeMillis()).name("马六").state(State.RETIRED).students(students()).build());
        teachers.add(Teacher.builder().id(System.currentTimeMillis()).name("猴七").state(State.VACATION).students(students()).build());
        teachers.add(Teacher.builder().id(System.currentTimeMillis()).name("猴七").state(State.VACATION).students(new ArrayList<>()).build());

        List<String> states = new ArrayList<>();
        states.add(State.VACATION.name());
        states.add(State.RETIRED.name());

        List<Student> collect = Optional.ofNullable(teachers)
                .orElseGet(ArrayList::new)
                .stream()
                .filter(teacher -> states.contains(teacher.getState().name()))
                .map(Teacher::getStudents)
                .flatMap(students -> Optional.ofNullable(students)
                        .orElseGet(ArrayList::new)
                        .stream()
                ).collect(Collectors.toList());

        System.err.println(collect.size());
        System.err.println(JSON.toJSONString(collect));

        List<Student> students = Optional.ofNullable(teachers)
                .stream()
                .flatMap(Collection::stream)
                .filter(teacher -> states.contains(teacher.getState().name()))
                .flatMap(teacher -> Optional.ofNullable(teacher.getStudents()).stream())
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

        System.err.println(students.size());
        System.err.println(JSON.toJSONString(students));

    }

    private List<Student> students(){
        List<Student> students = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Student student = Student.builder().id(System.currentTimeMillis()).name("战三" + i).build();
            students.add(student);
        }
        return students;
    }

    @Test
    public void test2(){
        List<Teacher> teachers = new ArrayList<>();
        teachers.add(Teacher.builder().id(System.currentTimeMillis()).name("张三").state(State.NORMAL).students(students()).build());
        teachers.add(Teacher.builder().id(System.currentTimeMillis()).name("李四").state(State.VACATION).students(students()).build());
        teachers.add(Teacher.builder().id(System.currentTimeMillis()).name("王五").state(State.RETIRED).students(students()).build());
        teachers.add(Teacher.builder().id(System.currentTimeMillis()).name("马六").state(State.RETIRED).students(students()).build());
        teachers.add(Teacher.builder().id(System.currentTimeMillis()).name("猴七").state(State.VACATION).students(students()).build());

        List<User> users = Optional.ofNullable(teachers)
                .map(teachers1 -> teachers1.stream().map(teacher -> User.builder().id(teacher.getId()).build()).collect(Collectors.toList()))
                .orElse(Collections.emptyList());

        List<User> collect = Optional.ofNullable(teachers)
                .stream()
                .flatMap(Collection::stream)
                .map(teacher -> User.builder().id(teacher.getId()).build())
                .collect(Collectors.toList());

        List<User> collect1 = Optional.ofNullable(teachers)
                .stream()
                .flatMap(teachers1 -> teachers1.stream())
                .map(teacher -> User.builder().id(teacher.getId()).build())
                .collect(Collectors.toList());

        System.err.println(collect);
    }
}
