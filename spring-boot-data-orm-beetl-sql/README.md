# spring-boot-orm-beetl-sql

> 此 demo 主要演示了 Spring Boot 如何整合 beetl sql 快捷操作数据库，使用的是beetl官方提供的beetl-framework-starter集成。集成过程不是十分顺利，没有其他的orm框架集成的便捷。

## 参考

- BeetlSQL官方文档：http://ibeetl.com/guide/#beetlsql
- 开源项目：https://gitee.com/yangkb/springboot-beetl-beetlsql
- 博客：https://blog.csdn.net/flystarfly/article/details/82752597