package com.sixbro.orm.beetl.mapper;

import com.sixbro.orm.beetl.entity.User;
import org.beetl.sql.core.mapper.BaseMapper;
import org.springframework.stereotype.Component;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2022/1/16 0:34
 */
@Component
public interface UserMapper extends BaseMapper<User> {
}
