package com.sixbro.orm.beetl.util;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2022/1/16 8:48
 */
public class Assert extends org.springframework.util.Assert{

    public static void equals(Object source, Object target) {
        if (!source.equals(target)) {
            throw new IllegalStateException("[Assertion failed] - this state invariant must be equals");
        }
    }
}
