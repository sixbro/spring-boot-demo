package com.sixbro.orm.beetl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2022/1/15 23:46
 */
@SpringBootApplication
public class ORMBeetlSQLApplication {

    public static void main(String[] args) {
        SpringApplication.run(ORMBeetlSQLApplication.class, args);
    }
}
