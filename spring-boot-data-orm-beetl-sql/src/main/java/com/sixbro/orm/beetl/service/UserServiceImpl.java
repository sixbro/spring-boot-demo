package com.sixbro.orm.beetl.service;

import cn.hutool.core.util.ObjectUtil;
import com.sixbro.orm.beetl.entity.User;
import com.sixbro.orm.beetl.mapper.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2022/1/16 0:36
 */
@Slf4j
@Service
public class UserServiceImpl implements UserService {


    private final UserMapper userMapper;

    @Autowired
    public UserServiceImpl(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    /**
     * 新增用户
     *
     * @param user 用户
     */
    @Override
    public User saveUser(User user) {
        userMapper.insert(user, true);
        return user;
    }

    /**
     * 批量插入用户
     *
     * @param users 用户列表
     */
    @Override
    public void saveUserList(List<User> users) {
        userMapper.insertBatch(users);
    }

    /**
     * 根据主键删除用户
     *
     * @param id 主键
     */
    @Override
    public void deleteUser(Long id) {
        userMapper.deleteById(id);
    }

    /**
     * 更新用户
     *
     * @param user 用户
     * @return 更新后的用户
     */
    @Override
    public User updateUser(User user) {
        if (ObjectUtil.isNull(user)) {
            throw new RuntimeException("用户id不能为null");
        }
        userMapper.updateTemplateById(user);
        return userMapper.single(user.getId());
    }

    /**
     * 查询单个用户
     *
     * @param id 主键id
     * @return 用户信息
     */
    @Override
    public User getUser(Long id) {
        return userMapper.single(id);
    }

    /**
     * 查询用户列表
     *
     * @return 用户列表
     */
    @Override
    public List<User> getUserList() {
        return userMapper.all();
    }

    /**
     * 分页查询
     *
     * @param currentPage 当前页
     * @param pageSize    每页条数
     * @return 分页用户列表
     */
    @Override
    public PageQuery<User> getUserByPage(Integer currentPage, Integer pageSize) {
        return userMapper.createLambdaQuery().page(currentPage, pageSize);
    }
}
