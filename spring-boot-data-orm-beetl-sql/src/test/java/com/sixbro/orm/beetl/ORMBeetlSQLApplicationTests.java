package com.sixbro.orm.beetl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.json.JSONUtil;
import com.google.common.collect.Lists;
import com.sixbro.orm.beetl.entity.User;
import com.sixbro.orm.beetl.service.UserService;
import com.sixbro.orm.beetl.util.Assert;
import lombok.extern.slf4j.Slf4j;
import org.beetl.sql.core.engine.PageQuery;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2022/1/16 0:38
 */
@Slf4j
@SpringBootTest
public class ORMBeetlSQLApplicationTests {
    @Autowired
    private UserService userService;

    @Test
    public void saveUser() {
        String salt = IdUtil.fastSimpleUUID();
        User user = User.builder().name("testSave3").password(SecureUtil.md5("123456" + salt)).salt(salt).email("testSave3@xkcoding.com").phoneNumber("17300000003").status(1).lastLoginTime(LocalDateTime.now()).createTime(LocalDateTime.now()).lastUpdateTime(LocalDateTime.now()).build();

        user = userService.saveUser(user);
        Assert.isTrue(ObjectUtil.isNotNull(user.getId()));
        log.debug("【user】= {}", user);
    }

    @Test
    public void saveUserList() {
        List<User> users = Lists.newArrayList();
        for (int i = 5; i < 15; i++) {
            String salt = IdUtil.fastSimpleUUID();
            User user = User.builder().name("testSave" + i).password(SecureUtil.md5("123456" + salt)).salt(salt).email("testSave" + i + "@xkcoding.com").phoneNumber("1730000000" + i).status(1).lastLoginTime(LocalDateTime.now()).createTime(LocalDateTime.now()).lastUpdateTime(LocalDateTime.now()).build();
            users.add(user);
        }
        userService.saveUserList(users);
        Assert.isTrue(userService.getUserList().size() > 2);
    }

    @Test
    public void deleteUser() {
        userService.deleteUser(1L);
        User user = userService.getUser(1L);
        Assert.isTrue(ObjectUtil.isNull(user));
    }

    @Test
    public void updateUser() {
        User user = userService.getUser(2L);
        user.setName("beetlSql 修改后的名字");
        User update = userService.updateUser(user);
        Assert.equals("beetlSql 修改后的名字", update.getName());
        log.debug("【update】= {}", update);
    }

    @Test
    public void getUser() {
        User user = userService.getUser(1L);
        Assert.notNull(user);
        log.debug("【user】= {}", user);
    }

    @Test
    public void getUserList() {
        List<User> userList = userService.getUserList();
        Assert.isTrue(CollUtil.isNotEmpty(userList));
        log.debug("【userList】= {}", userList);
    }

    @Test
    public void getUserByPage() {
        List<User> userList = userService.getUserList();
        PageQuery<User> userByPage = userService.getUserByPage(1, 5);
        Assert.equals(5, userByPage.getList().size());
        Assert.equals(userList.size(), userByPage.getTotalRow());
        log.debug("【userByPage】= {}", JSONUtil.toJsonStr(userByPage));
    }
}
