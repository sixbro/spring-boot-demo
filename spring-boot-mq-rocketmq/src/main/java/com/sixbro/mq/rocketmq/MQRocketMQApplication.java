package com.sixbro.mq.rocketmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-25 13:39
 */
@SpringBootApplication
public class MQRocketMQApplication {

    public static void main(String[] args) {
        SpringApplication.run(MQRocketMQApplication.class, args);
    }
}
