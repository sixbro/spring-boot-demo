# spring-boot-mq-rocketmq

> 此 demo 主要演示了 Spring Boot 如何集成 RocketMQ，并且演示了基于直接队列模式、分列模式、主题模式、延迟队列的消息发送和接收。