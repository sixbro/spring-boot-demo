package com.sixbro.data.mongodb.repository;

import com.sixbro.data.mongodb.model.Article;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * <p>
 *  文章 Dao
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-24 17:51
 */
public interface ArticleRepository extends MongoRepository<Article, Long> {

    /**
     * 根据标题模糊查询
     *
     * @param title 标题
     * @return 满足条件的文章列表
     */
    List<Article> findByTitleLike(String title);
}
