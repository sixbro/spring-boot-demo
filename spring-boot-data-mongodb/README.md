# spring-boot-data-mongodb

> 此 demo 主要演示了 Spring Boot 如何集成 MongoDB，使用官方的 starter 实现增删改查。

## 注意

作者编写本demo时，MongoDB 最新版本为 `4.1`，使用 docker 运行，下面是所有步骤：

1. 下载镜像：`docker pull mongo:4.1`
2. 运行容器：`docker run -d -p 27017:27017 -v /Users/yangkai.shen/docker/mongo/data:/data/db --name mongo-4.1 mongo:4.1`
3. 停止容器：`docker stop mongo-4.1`
4. 启动容器：`docker start mongo-4.1`


## 参考

1. Spring Data MongoDB 官方文档：https://docs.spring.io/spring-data/mongodb/docs/2.6.0/reference/html/
2. MongoDB 官方镜像地址：https://hub.docker.com/_/mongo
3. MongoDB 官方快速入门：https://docs.mongodb.com/manual/tutorial/getting-started/
4. MongoDB 官方文档：https://docs.mongodb.com/manual/