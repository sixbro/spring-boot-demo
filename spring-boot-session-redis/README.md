# spring-boot-session-redis

> 此 demo 主要演示了 Spring Boot 如何通过 Spring Session 实现Session共享、重启程序Session不失效。

## 测试

> 测试 重启程序，Session 不失效的场景

1. 打开浏览器，访问首页：http://localhost:8080/demo/page/index
2. 最开始未登录，所以会跳转到登录页：http://localhost:8080/demo/page/login?redirect=true 然后点击登录按钮
3. 登录之后，跳转回首页，此时可以看到首页显示token信息。
4. 重启程序。不关闭浏览器，直接刷新首页，此时不跳转到登录页。测试成功！

## 参考

- Spring Session 官方文档：https://docs.spring.io/spring-session/docs/current/reference/html5/guides/boot-redis.html#updating-dependencies