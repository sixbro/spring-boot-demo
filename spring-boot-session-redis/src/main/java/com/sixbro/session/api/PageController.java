package com.sixbro.session.api;

import com.sixbro.session.constants.CacheConstants;
import org.springframework.stereotype.Controller;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.UUID;

/**
 * <p>
 * 页面跳转 Controller
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2020/6/26 15:20
 */
@Controller
@RequestMapping(path = {"/page"})
public class PageController {

    /**
     * 跳转到 首页
     *
     * @param request 请求
     */
    @GetMapping("/index")
    public ModelAndView index(HttpServletRequest request) {
        ModelAndView view = new ModelAndView();
        String token = (String) request.getSession().getAttribute(CacheConstants.SESSION_KEY);
        view.setViewName("index");
        view.addObject("token", token);
        return view;
    }

    /**
     * 跳转到 登录页
     *
     * @param redirect 是否是跳转回来的
     */
    @GetMapping("/login")
    public ModelAndView login(Boolean redirect) {
        ModelAndView view = new ModelAndView();

        if (!ObjectUtils.isEmpty(redirect) && redirect == true) {
            view.addObject("message", "请先登录！");
        }
        view.setViewName("login");
        return view;
    }

    @GetMapping("/doLogin")
    public String doLogin(HttpSession session) {
        session.setAttribute(CacheConstants.SESSION_KEY, UUID.randomUUID());

        return "redirect:/page/index";
    }
}
