package com.sixbro.session.api;

import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @Author: Mr.Lu
 * @Since: 2020/6/16 17:05
 */
@AllArgsConstructor
@RestController
@RequestMapping("/index")
public class IndexController {
    private static final Logger _logger = LoggerFactory.getLogger(IndexController.class);

    private final HttpSession session;
    private final HttpServletRequest request;


    /**
     * 登录页(shiro配置需要两个/login 接口,一个是get用来获取登陆页面,一个用post用于登录)
     * @return
     */
    @GetMapping(value = "/user")
    public String login() {

        _logger.info("**********************************************************************");
        _logger.info("request.getRequestedSessionId()  " + request.getRequestedSessionId());
        _logger.info("**********************************************************************");

        _logger.info("**********************************************************************");
        _logger.info( "session.getId() = " + session.getId() );
        _logger.info("**********************************************************************");


        Cookie[] cookies = request.getCookies();
        if(cookies == null) {
            return "login";
        }
        _logger.info("**********************************************************************");
        for (Cookie cookie : cookies) {
            _logger.info("cookie.getComment() = " + cookie.getComment());
            _logger.info("cookie.getName() = " + cookie.getName());
            _logger.info("cookie.getValue() = " + cookie.getValue());
        }
        _logger.info("**********************************************************************");
        return "login";
    }
}
