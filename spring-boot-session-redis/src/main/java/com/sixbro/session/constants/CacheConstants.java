package com.sixbro.session.constants;

/**
 * <p>
 *  常量池
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-17 15:49
 */
public interface CacheConstants {
    /**
     * session保存的key
     */
    String SESSION_KEY = "key:session:token";
}
