package com.sixbro.session;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 *  自定义异常处理类
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-17 15:30
 */
@Slf4j
public class ExceptionResolver implements HandlerExceptionResolver {

    @Override
    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        log.info("********  自定义异常处理类 {} *********", ExceptionResolver.class.getName());

        //如果是shiro无权操作，因为shiro 在操作auno等一部分不进行转发至无权限url
        ex.printStackTrace();

        return new ModelAndView("unauth","msg",ex.getMessage());
    }
}
