package com.sixbro.https;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-20 17:24
 */
@SpringBootApplication
public class HttpsApplication {

    public static void main(String[] args) {
        SpringApplication.run(HttpsApplication.class, args);
    }
}
