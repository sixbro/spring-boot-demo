package com.sixbro.dynamic.datasource.mapper;

import com.sixbro.dynamic.datasource.config.MyMapper;
import com.sixbro.dynamic.datasource.model.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  用户 Mapper
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-20 15:04
 */
@Mapper
public interface UserMapper extends MyMapper<User> {
}
