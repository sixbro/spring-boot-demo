package com.sixbro.dynamic.datasource.config;

import tk.mybatis.mapper.annotation.RegisterMapper;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * <p>
 *  通用 mapper 自定义 mapper 文件
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-20 15:04
 */
@RegisterMapper
public interface MyMapper<T> extends Mapper<T>, MySqlMapper<T> {
}
