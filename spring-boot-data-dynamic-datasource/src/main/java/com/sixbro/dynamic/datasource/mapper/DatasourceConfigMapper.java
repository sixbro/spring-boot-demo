package com.sixbro.dynamic.datasource.mapper;

import com.sixbro.dynamic.datasource.config.MyMapper;
import com.sixbro.dynamic.datasource.model.DatasourceConfig;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  数据源配置 Mapper
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-20 15:23
 */
@Mapper
public interface DatasourceConfigMapper extends MyMapper<DatasourceConfig> {
}
