package com.sixbro.websocket.netty.web;

import com.alibaba.fastjson.JSON;
import com.sixbro.websocket.netty.common.ChatConstants;
import com.sixbro.websocket.netty.module.UserInfo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/19 16:10
 */
@Controller
@RequestMapping(path = {"/chat"})
public class ChatController {

    /**
     * 跳转到交谈聊天页面
     *
     * @param token
     * @param model
     * @return
     */
    @RequestMapping(value = "list", method = RequestMethod.GET)
    public String talk(String token, Model model) {
        model.addAttribute("token", token);
        return "chat";
    }

    @ResponseBody
    @RequestMapping(value = "users", method = RequestMethod.GET, produces = {"application/json; charset=UTF-8", "text/plain"})
    public String users(String token) {
        Map<String, UserInfo> onlines = ChatConstants.onlines;
        UserInfo cur = onlines.get(token);

        Map<String, Object> map = new HashMap<>(2);
        map.put("curName", cur != null ? cur.getCode() : "");
        map.put("users", onlines);
        return JSON.toJSONString(map);
    }
}
