package com.sixbro.websocket.netty.module;

import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author: Mr.Lu
 * @since: 2022/1/16 11:42
 */
public class UserInfo implements Serializable {

    private static final long serialVersionUID = 3562768188264006800L;
    public static Map<String, UserInfo> map = new ConcurrentHashMap<>();

    private Long id;

    private String code;

    private String phone;

    private String password;

    private String avatar;

    public UserInfo() {
    }

    public UserInfo(String phone) {
        this.phone = phone;
        this.id = System.currentTimeMillis();
    }

    public Long getId() {
        return id;
    }

    public UserInfo setId(Long id) {
        this.id = id;
        return this;
    }

    public String getCode() {
        return code;
    }

    public UserInfo setCode(String code) {
        this.code = code;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public UserInfo setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public UserInfo setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getAvatar() {
        return avatar;
    }

    public UserInfo setAvatar(String avatar) {
        this.avatar = avatar;
        return this;
    }
}
