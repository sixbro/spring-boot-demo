package com.sixbro.websocket.netty.web;

import com.sixbro.websocket.netty.module.UserInfo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @Author: Mr.Lu
 * @Date: 2020/9/19 16:11
 */
@Controller
public class LoginController {

    /**
     * 跳转登录页面
     *
     * @return
     */
    @RequestMapping(value = {"", "/", "index"}, method = RequestMethod.GET)
    public String index() {
        return "index";
    }

    /**
     * 用户登录
     *
     * @param user
     * @return
     */
    @RequestMapping(value = "login", method = RequestMethod.POST)
    public String doLogin(UserInfo user) {
        UserInfo.map.put(user.getPhone(), user);
        //return "redirect:/msg/list?token="+user.getPhone();
        return "redirect:/chat/list?token=" + user.getPhone();
    }
}
