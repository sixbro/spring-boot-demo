package com.sixbro.websocket.netty.module;

import com.alibaba.fastjson.annotation.JSONField;

import java.time.LocalDateTime;
import java.util.Map;

/**
 * @author: Mr.Lu
 * @since: 2022/1/16 11:42
 */
public class Message {

    /**
     * 发送消息则
     */
    private UserInfo from;

    /**
     * 接收者列表
     */
    private Map<String, UserInfo> to;

    /**
     * 发送内容
     */
    private String message;

    /**
     * 发送时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    public Message() {
    }

    public Message(UserInfo from, String message) {
        this.message = message;
    }

    public UserInfo getFrom() {
        return from;
    }

    public Message setFrom(UserInfo from) {
        this.from = from;
        return this;
    }

    public Map<String, UserInfo> getTo() {
        return to;
    }

    public Message setTo(Map<String, UserInfo> to) {
        this.to = to;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public Message setMessage(String message) {
        this.message = message;
        return this;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public Message setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
        return this;
    }
}
