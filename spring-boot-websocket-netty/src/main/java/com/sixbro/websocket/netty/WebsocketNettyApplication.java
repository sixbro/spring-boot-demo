package com.sixbro.websocket.netty;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2022/1/16 11:42
 */
@SpringBootApplication
public class WebsocketNettyApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebsocketNettyApplication.class, args);
    }
}
