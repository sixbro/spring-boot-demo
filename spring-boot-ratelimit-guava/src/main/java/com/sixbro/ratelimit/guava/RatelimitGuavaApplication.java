package com.sixbro.ratelimit.guava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-15 15:59
 */
@SpringBootApplication
public class RatelimitGuavaApplication {

    public static void main(String[] args) {
        SpringApplication.run(RatelimitGuavaApplication.class, args);
    }
}
