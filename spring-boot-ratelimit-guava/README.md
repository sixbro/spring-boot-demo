## spring-boot-ratelimit-guava

> 此 demo 主要演示了 Spring Boot 项目如何通过 AOP 结合 Guava 的 RateLimiter 实现限流，旨在保护 API 被恶意频繁访问的问题。


## 参考

- [限流原理解读之guava中的RateLimiter](https://juejin.im/post/5bb48d7b5188255c865e31bc)

- [使用Guava的RateLimiter做限流](https://my.oschina.net/hanchao/blog/1833612)