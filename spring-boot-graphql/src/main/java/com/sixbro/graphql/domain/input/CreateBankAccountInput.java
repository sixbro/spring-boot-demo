package com.sixbro.graphql.domain.input;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021/10/19 11:30
 */
@Data
public class CreateBankAccountInput {
    @NotBlank
    String firstName;
    int age;
}
