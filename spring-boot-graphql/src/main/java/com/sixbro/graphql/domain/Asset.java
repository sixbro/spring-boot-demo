package com.sixbro.graphql.domain;

import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021/10/19 10:57
 */
@Setter
@Component
public class Asset {
    UUID id;
}
