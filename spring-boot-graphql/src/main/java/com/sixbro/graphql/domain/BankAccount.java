package com.sixbro.graphql.domain;

import lombok.Builder;
import lombok.Value;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.UUID;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021/10/18 15:45
 */
@Builder
@Value
public class BankAccount {
    UUID id;
    Client client;
    Currency currency;
    OffsetDateTime createdAt;
    LocalDate createdOn;
}
