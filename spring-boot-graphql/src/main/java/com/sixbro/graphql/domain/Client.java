package com.sixbro.graphql.domain;

import lombok.Builder;
import lombok.Setter;
import lombok.Value;

import java.util.UUID;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021/10/18 17:22
 */
@Setter
@Builder
public class Client {
    UUID id;
    String firstName;
    String lastName;
}
