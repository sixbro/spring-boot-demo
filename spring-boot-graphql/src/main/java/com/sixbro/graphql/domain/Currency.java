package com.sixbro.graphql.domain;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021/10/18 15:46
 */
public enum Currency {
    RMB, USD
}
