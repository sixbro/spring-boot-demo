package com.sixbro.graphql.resolver.query;

import com.sixbro.graphql.domain.BankAccount;
import com.sixbro.graphql.domain.Client;
import com.sixbro.graphql.domain.Currency;
import graphql.kickstart.tools.GraphQLQueryResolver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021/10/18 15:32
 */
@Slf4j
@Component
public class BankAccountResolver implements GraphQLQueryResolver {

    public BankAccount bankAccount(UUID id) {
        log.info("Retrieve bank account, id {}", id);

        return BankAccount.builder()
                .id(id)
                .currency(Currency.RMB)
                .build();
    }
}
