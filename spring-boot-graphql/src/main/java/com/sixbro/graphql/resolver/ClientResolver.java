package com.sixbro.graphql.resolver;

import com.sixbro.graphql.domain.BankAccount;
import com.sixbro.graphql.domain.Client;
import graphql.GraphQLException;
import graphql.execution.DataFetcherResult;
import graphql.kickstart.execution.error.GenericGraphQLError;
import graphql.kickstart.tools.GraphQLResolver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021/10/19 9:35
 */
@Slf4j
@Component
public class ClientResolver implements GraphQLResolver<BankAccount> {

    private final ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

    public CompletableFuture<Client> client(BankAccount bankAccount) {

        // throw new GraphQLException("DB connection error");

        return CompletableFuture.supplyAsync(
                () -> {
                    log.info("Retrieving the client data for bank account {}", bankAccount);
                    return Client.builder()
                            .id(UUID.randomUUID())
                            .firstName("William")
                            .lastName("Bobo1")
                            .build();
                },
                executorService
        );
    }
}
