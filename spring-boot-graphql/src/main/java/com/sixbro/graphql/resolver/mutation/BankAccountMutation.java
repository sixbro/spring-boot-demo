package com.sixbro.graphql.resolver.mutation;

import com.sixbro.graphql.domain.BankAccount;
import com.sixbro.graphql.domain.Currency;
import com.sixbro.graphql.domain.input.CreateBankAccountInput;
import graphql.kickstart.tools.GraphQLMutationResolver;
import graphql.schema.DataFetchingEnvironment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.UUID;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021/10/19 11:32
 */
@Slf4j
@Component
@Validated
public class BankAccountMutation implements GraphQLMutationResolver {
    public BankAccount createBankAccount(@Valid CreateBankAccountInput input, DataFetchingEnvironment env) {
      log.info("Createing bank accout for {}", input);

        if (env.getSelectionSet().contains("xyz")) {
            // custom logic

        }

      return BankAccount.builder()
              .id(UUID.randomUUID())
              .currency(Currency.RMB)
              .createdAt(OffsetDateTime.now())
              .createdOn(LocalDate.now())
              .build();
    }
}
