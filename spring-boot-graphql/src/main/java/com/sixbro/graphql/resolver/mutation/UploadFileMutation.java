package com.sixbro.graphql.resolver.mutation;

import com.sixbro.graphql.domain.BankAccount;
import graphql.kickstart.servlet.context.DefaultGraphQLServletContext;
import graphql.kickstart.tools.GraphQLMutationResolver;
import graphql.schema.DataFetchingEnvironment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021/10/19 16:48
 */
@Slf4j
@Component
public class UploadFileMutation implements GraphQLMutationResolver {
    public UUID uploadFile(DataFetchingEnvironment env) {
        DefaultGraphQLServletContext ctx = env.getContext();

        ctx.getFileParts().forEach(part -> log.info("Uploading {}, size: {}", part.getSubmittedFileName(), part.getSize()));

        return UUID.randomUUID();
    }
}
