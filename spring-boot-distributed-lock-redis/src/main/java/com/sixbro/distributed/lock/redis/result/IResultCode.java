package com.sixbro.distributed.lock.redis.result;

/**
 * <p>
 *  统一状态码接口
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-20 14:20
 */
public interface IResultCode {

    /**
     * 获取状态码
     *
     * @return 状态码
     */
    int getCode();

    /**
     * 获取返回消息
     *
     * @return 返回消息
     */
    String getMessage();
}
