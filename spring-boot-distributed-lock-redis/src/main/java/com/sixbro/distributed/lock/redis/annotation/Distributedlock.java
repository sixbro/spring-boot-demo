package com.sixbro.distributed.lock.redis.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>
 *  分布式锁
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-20 11:52
 */
// 作用到方法上
@Target({ElementType.METHOD})
// 运行时有效
@Retention(RetentionPolicy.RUNTIME)
public @interface Distributedlock {

    long lockTime() default 1000;
}
