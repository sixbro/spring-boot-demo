package com.sixbro.distributed.lock.redis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-20 11:48
 */
@SpringBootApplication
public class DistributedLockRedisApplication {

    public static void main(String[] args) {
        SpringApplication.run(DistributedLockRedisApplication.class, args);
    }
}
