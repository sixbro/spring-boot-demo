package com.sixbro.distributed.lock.redis.exception;

import com.sixbro.distributed.lock.redis.result.IResultCode;

/**
 * <p>
 *  业务异常
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-20 14:27
 */
public class ApiException extends RuntimeException {

    private int code;
    private String message;

    public ApiException(int code, String message){
        super(message);
        this.code = code;
        this.message = message;
    }

    public ApiException(IResultCode error){
        super(error.getMessage());
        this.code = error.getCode();
        this.message = error.getMessage();
    }

    public int getCode() {
        return code;
    }

    public ApiException setCode(int code) {
        this.code = code;
        return this;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public ApiException setMessage(String message) {
        this.message = message;
        return this;
    }
}
