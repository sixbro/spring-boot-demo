# Redis分布式锁的正确实现方式

## 前言

分布式锁一般有三种实现方式：
1. 基于数据库实现乐观锁；
2. 基于缓存（Redis，Memcached）实现分布式锁
3. 基于Zookeeper实现分布式锁

> 本篇将介绍第二种方式，基于Redis实现分布式锁。虽然网上已经有各种介绍Redis分布式锁的实现，然而他们的实现却有着各种各样的问题，为了避免误人子弟，本篇将详细介绍如何正确地实现Redis分布式锁。

## 可靠性

首先，为了确保分布式锁可用，我们至少要确保锁的实现同时满足以下四个条件：

1. 互斥性。在任意时刻，只有一个客户端能持有锁。
2. 不会发生死锁。即使有一个客户端在持有锁的期间崩溃而没有主动解锁，也能保证后续其他客户端能加锁。
3. 具有容错性。只要大部分的Redis节点正常运行，客户端就可以加锁和解锁。
4. 解铃还须系铃人。加锁和解锁必须是同一个客户端，客户端自己不能把别人加的锁给解了。

## 代码实现

### 组件依赖

首先我们要通过Maven引入Jedis开源组件，在pom.xml文件加入下面的代码：

```xml
<!-- 对象池，使用redis时必须引入 -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-redis</artifactId>
</dependency>
<dependency>
    <groupId>org.apache.commons</groupId>
    <artifactId>commons-pool2</artifactId>
</dependency>
```

### 加锁代码

#### 正确姿势

Talk is cheap, show me the code。先展示代码，再带大家慢慢解释为什么这样实现：