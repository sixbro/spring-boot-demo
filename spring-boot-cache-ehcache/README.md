## spring-boot-cache-ehcache

> 此 demo 主要演示了 Spring Boot 如何集成 ehcache 使用缓存。

## 缓存注解说明

1. `@CacheConfig`  
    这个注解在类上使用，用来指定该类中所有方法使用的全局缓存名称。  
  
2. `@Cacheable`  
    这个注解一般在查询方法上使用，表示将一个方法的返回值缓存起来，默认缓存的 key 为所有参数的值（可通过 key 或 keyGenerator 修改），缓存的 value 为方法的返回值。

    如果方法有多个参数时，默认就使用多个参数来做 key ，如果只需要其中某一个参数做 key ，则可以在 `@Cacheable` 注解中，通过 `key` 属性来指定，如 `@Cacheable(key = "#id")` 。如果对 key 有复杂的要求，可以自定义 `keyGenerator` 。 Spring Cache 默认中提供了 `root` 对象，可以在不定义 keyGenerator 的情况下实现一些复杂的效果，如下：

   | 属性 | 描述 | 示例 |
   | --- | --- | --- |
   | methodName   | 当前方法名                | #root.methodName    |
   | method       | 当前方法                  | #root.method.name   |
   | target       | 当前被调用的对象           | #root.target        |
   | targetClass  | 当前被调用的对象的class    | #root.targetClass    |
   | args         | 当前方法参数数组           | #root.args[0]        |
   | caches       | 当前被调用的方法使用的Cache | #root.caches[0].name |

   也可以通过 keyGenerator 自定义 key ，如 `@Cacheable(keyGenerator = "myKeyGenerator")` 。需要新增 `MyKeyGenerator` 配置类，如下：
    
    ```java
    @Component
    public class MyKeyGenerator implements KeyGenerator {
        @Override
        public Object generate(Object o, Method method, Object... objects) {
            // 自定义缓存的 key
            return method.getName() + ":" + Arrays.toString(objects);
        }
    }
    ```

3. `@CacheEvict`  
    这个注解一般在删除方法上使用，当数据库中的数据删除后，相关的缓存数据也要自动清除。当然也可以配置按照某种条件删除（ `condition` 属性）或者配置清除所有缓存（ `allEntries` 属性）。

4. `@CachePut`  
   这个注解一般在`更新方法`上使用，当数据库中的数据更新后，缓存中的数据也要跟着更新，使用该注解，可以将方法的返回值自动更新到已经存在的 key 上。


## 参考

- Ehcache 官网：http://www.ehcache.org/documentation/
- Spring Boot 官方文档：https://docs.spring.io/spring-boot/docs/2.6.1/reference/htmlsingle/#io.caching.provider.ehcache2
- 博客：https://juejin.im/post/5b308de9518825748b56ae1d