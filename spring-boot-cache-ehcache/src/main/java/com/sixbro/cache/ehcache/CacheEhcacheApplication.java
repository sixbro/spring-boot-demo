package com.sixbro.cache.ehcache;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-17 13:41
 */
@EnableCaching
@SpringBootApplication
public class CacheEhcacheApplication {

    public static void main(String[] args) {
        SpringApplication.run(CacheEhcacheApplication.class, args);
    }
}
