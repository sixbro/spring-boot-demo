package com.sixbro.orm.jdbc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2022/1/16 9:14
 */
@SpringBootApplication
public class ORMJDBCApplication {

    public static void main(String[] args) {
        SpringApplication.run(ORMJDBCApplication.class, args);
    }
}
