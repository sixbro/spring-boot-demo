package com.sixbro.orm.jdbc.constant;

/**
 * <p>
 * 常量池
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2022/1/16 9:14
 */
public interface Const {
    /**
     * 加密盐前缀
     */
    String SALT_PREFIX = "::SpringBootSamples::";

    /**
     * 逗号分隔符
     */
    String SEPARATOR_COMMA = ",";
}
