package com.sixbro.aop.api;

import com.sixbro.aop.annotation.Signup;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *
 * </p>
 *
 * @author: Mr.Lu
 * @since: 2021-12-17 15:19
 */
@RestController
public class UserController {

    @RequestMapping(value = "/signup")
    @Signup(desc = "signup")
    public Object signup(){
        return "注册成功";
    }

    @RequestMapping(value = "/login")
    public Object login(){
        return "登录成功";
    }

    @RequestMapping(value = "/logout")
    public Object logout(){
        return "退出成功";
    }
}
