package com.sixbro.aop.aspect;

import com.sixbro.aop.annotation.Signup;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * @Author: Mr.Lu
 * @Since: 2020/6/16 13:47
 */
@Aspect
@Component
public class SignupAspect {

    @Pointcut(value = "@annotation(com.sixbro.aop.annotation.Signup)")
    public void access() {
    }

    @Before("access()")
    public void deBefore(JoinPoint joinPoint) throws Throwable {
        System.out.println("signup before");
    }

    @Around("@annotation(signup)")
    public Object around(ProceedingJoinPoint joinPoint, Signup signup) {
        //获取注解里的值
        System.out.println("signup around:" + signup.desc());
        try {
            return joinPoint.proceed();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return null;
        }
    }
}
