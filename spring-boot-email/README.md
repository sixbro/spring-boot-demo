## spring-boot-email

> 此 demo 主要演示了 Spring Boot 如何整合邮件功能，包括发送简单文本邮件、HTML邮件（包括模板HTML邮件）、附件邮件、静态资源邮件。

## 
![163免费邮客户端设置的POP3、SMTP、IMAP地址](assets/1592877094.png)

## 参考

- Spring Boot 官方文档：https://docs.spring.io/spring-boot/docs/2.6.1/reference/htmlsingle/#io.email
- Spring Boot 官方文档：https://docs.spring.io/spring-framework/docs/5.3.13/reference/html/integration.html#mail